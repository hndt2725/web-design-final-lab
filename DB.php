<?php 
session_start();

class DB
{
    static protected $connection;
    const DB_TYPE = "mysql";
    const DB_HOST = "localhost";
    const DB_NAME = "web1";
    const USER_NAME = "root";
    const USER_PASSWORD = "";


    static public function getconnection()
    {
        if(static::$connection == null){
            try{
                static::$connection = new PDO(dsn:self::DB_TYPE . ":host=" . self::DB_HOST . ";dbname=" . self::DB_NAME, username: self::USER_NAME, password: self::USER_PASSWORD);
        }   catch (Exception $exception) {
                throw new Exception(message: "connection fail");
            }
        }
        return static::$connection;
    }
    
    static public function execute($sql, $data = [])
    {
        $statement = DB::getconnection()->prepare($sql);
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $statement-> execute($data);
        $result = [];

        while ($item = $statement->fetch() ) {
            $result[] = $item;

        }
        return $result;
    }   
}


?>