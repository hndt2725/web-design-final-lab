
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./validate-form-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title> SIGN UP </title>
</head>
<body>
    <div class="main">

        <form action="signup.php" method="POST" class="form" id="form-1">
          <h3 class="heading">Welcome</h3>
          <p class="desc">Make an account to become our member! </p>
      
          <div class="spacer"></div>
      
          <div class="form-group">
            <label for="fullname" class="form-label">Full name</label> 
            <input id="fullname" name="fullname" type="text" placeholder="Ex: Hồng Nhung" class="form-control">
            <span class="form-message"></span>
          </div>
      
          <div class="form-group">
            <label for="email" class="form-label">Email</label>
            <input id="email" name="email" type="text"  placeholder="Ex: email@gmail.com" class="form-control">
            <span class="form-message"></span>
          </div>
          
          <div class="form-group">
            <label for="username" class="form-label">Username</label>
            <input id="username" name="username" type="text"  placeholder="Ex: Nhung2711" class="form-control">
            <span class="form-message"></span>
          </div>

          
          <div class="form-group">
            <label for="password" class="form-label">Password</label>
            <input id="password" name="password" type="password" placeholder="12345678" class="form-control">
            <span class="form-message"></span>
          </div>
          
          <div class="form-group">
            <label for="password_confirmation" class="form-label">Password</label>
            <input id="password_confirmation" name="password_confirmation" type="password" class="form-control">
            <span class="form-message"></span>
          </div>

          <div class="form-group">
            <label for="address" class="form-label">Address</label>
            <input id="address" name="address" type="text" class="form-control">

            <span class="form-message"></span>
          </div>

          <div class="form-group">
            <label for="" class="form-label">Gender</label>
            <div >
              <input name="gender" id="male" type="radio" value='male' class="form-control">
              <label for="male" >Male</label>
            </div>
            <div >
              <input name="gender"  id="female" type="radio" value='female' class="form-control">
              <label for="female" >Femail</label>
            </div>
        
            <span class="form-message"></span>
          </div>
          
          

          <button class="form-submit" name="btn-reg" value="btn-reg">Signin</button>
        </form>
      
      </div>

    <!-- <script src="./validate-form.js"></script>

    <script>
      Validator({
        form: "#form-1",
        formGroupSelector: '.form-group', // thay tên class tương ứng là được
        errorSelector:'.form-message' , // -> .js : thay vì dùng "form-message" thì dùng "option.errorSelector"
        rules: [   
          Validator.isRequired('#fullname','Vui lòng nhập đầy đủ tên của bạn'), // chạy 1 hàm 
          Validator.isRequired('#email', 'Vui lòng nhập email'),
          Validator.isEmail('#email'),
          Validator.isRequired('#username', 'Vui lòng nhập tên người dùng'),
          Validator.isRequired('#address', 'Vui lòng nhập địa chỉ'),
          Validator.minLength('#password', 6),
          Validator.isRequired('#password_confirmation'),
          Validator.isConfirmed('#password_confirmation', function(){
            return document.querySelector('#form-1 #password').value ; // bắt buộc phải nhập đúng password trong form 1
          }, 'Mật khẩu nhập lại không chính xác'),
          Validator.isRequired('input[name="gender"]'),
        ],
      });
    </script> -->

</body>
</html>
