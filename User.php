<?php 
include_once "./DB.php";
// include_once "./helper.php";

    function validate ($request, $keys)
    {
        $result = []; 

        foreach ($keys as $key)
        {
            $error = validateField($request, $key);

            if ($error != "")
            {
                $result[$key] = $error;
            }
        }
    return $result;
    }

    function validateField ($request, $key)
    {
        return isset($request[$key]) && $request[$key] != "" ? "" : "$key is required";
    }



class User
{
    static public function all()
    {
        $sql = "SELECT * from contact"; 
        $users = DB::execute($sql);

        return $users;    
    }
    
    static public function create($dataCreate)
    {
        $sql = "INSERT INTO contact (name, email, phoneNumber, message) values (:name, :email, :phoneNumber, :message)";
        DB::execute($sql, $dataCreate);   
    }

    static public function find($id)
    {
        $sql = "SELECT * FROM contact WHERE id=:id"; 
        $dataFind = ['id' => $id];

        $user = DB::execute($sql, $dataFind); 

        return count($user) > 0 ? $user[0] :[];
    }

    static public function update($dataUpdate) 
    {
        $sql = "UPDATE contact set name=:name, email=:email, phoneNumber=:phoneNumber, message=:message WHERE id=:id";
        DB::execute($sql, $dataUpdate);
    }

    static public function destroy($id)
    {
        $sql = "DELETE FROM contact  WHERE id=:id";
        $dataDelete = ['id'=>$id];
        DB::execute($sql, $dataDelete);
    }
}

?> 