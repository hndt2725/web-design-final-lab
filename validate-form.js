
function Validator(options){

    function getParent(element, selector){ // tìm lớp cha của form-control
        while (element.parentElement) {
            if (element.parentElement.matches(selector)){
                return element.parentElement;
            } 
            element = element.parentElement;
        }
    }

    var selectorRules = {}; // lưu tất cả các rule vào hàm này


    function validate(inputElement, rule) {   // hàm thực hiện việc hiện lỗi hoặc bỏ lỗi đi
        
        var errorMessage;
        var errorElement = getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector)


        //Lấy ra các rule của selector
        var rules = selectorRules [rule.selector];  // thu được rule của chính cái mình blur ra ngoài
        
        //Lặp qua từng rules và kiểm tra
        for (var i = 0; i < rules.length; ++i) {  
            switch (inputElement.type) {
                case 'checkbox':
                case 'radio':
                    errorMessage = rules[i](
                        formElement.querySelector(rule.selector + ':checked') //////////////////////////
                    );
                    break; 
                default: 
                errorMessage = rules[i](inputElement.value);
            }
            
            if (errorMessage) break ; // nếu có lỗi thì sẽ thoát khỏi vòng lặp, dừng kiểm tra
        }


            if (errorMessage){
                errorElement.innerText = errorMessage; // đưa error message ra ngoài
                getParent(inputElement, options.formGroupSelector).classList.add('invalid'); // thêm màu đỏ khi có lỗi 
                // console.log(getParent(inputElement, options.formGroupSelector).querySelector('.form-message')); 
                    // thẻ con . thẻ cha . 

            } else {
                errorElement.innerText = ''; // gán bằng chuỗi rống -> xóa error message khi không có lỗi
                getParent(inputElement, options.formGroupSelector).classList.remove('invalid'); // -> xóa màu đỏ khi không có lỗi
            }

            return ! errorMessage; 
    }


    // lấy element của form cần validate 
    var formElement = document.querySelector(options.form);

    if (formElement) {

        // lắng nghe sự kiện submit form
        formElement.onsubmit = function (e) { 
            e.preventDefault ();

            var isFormValid = true;


        //thực hiện lặp qua từng rule và validate
        options.rules.forEach(function(rule){
                var inputElement = formElement.querySelector(rule.selector);
                var isValid = validate (inputElement, rule);
                if (!isValid) {
                    isFormValid = false;
                }
        });

        // TRƯỜNG HỢP SUBBMIT VỚI JAVASCRIPT
        if (isFormValid) {
            if(typeof options.onSubmit === 'function') {
                var enableInputs = formElement.querySelectorAll('[name]');  //:not([disabled]
                var formValues = Array.from(enableInputs).reduce(function (values, input){
                    
                    switch(input.type) {
                        case 'radio':
                            values[input.name] = formElement.querySelector('input[name="' + input.name + '"]:checked').value;
                            break;
                        case 'checkbox':
                            if(!input.matches(':checked')) {
                                values[input.name] = '';
                                return values; 
                            }

                            if(!Array.isArray(values[input.name])){
                                values[input.name] = []; 
                            }
                            
                            values[input.name].push(input.value);             
                            break;

                        default:
                            values[input.name] = input.value;
                    }

                    return values;
                }, {});

                options.onSubmit(formValues);
            } else {  // TRƯỜNG HỢP SUBMIT với hành vi mặc định của trình duyệt
                formElement.submit();
            }
        }

        } 


        // xử lí lặp qua mỗi rule và xử lý: lắng nghe sự kiện blur, input 
        options.rules.forEach(function(rule){

            //Lưu lại các rule cho mỗi input
            
            if(Array.isArray(selectorRules[rule.selector])) {
                selectorRules[rule.selector].push(rule.test);   // đưa rule vào mảng
            }    else {
                    selectorRules[rule.selector] = [rule.test];   //gán là 2 mảng trong đó chứa rule
            
            }

            var inputElements = formElement.querySelectorAll(rule.selector); // tim input trong form
            
            Array.from(inputElements).forEach(function(inputElement){

                if (inputElement) {   // neu ton tai cai element
                    
                    inputElement.onblur = function() { // -> xử lí trường hợp blur ra ngoài khỏi input
                        /* console.log('blur '+ rule.selector); => blur ra khỏi cái gì + tên vừa blur
                        hoac: 
                        console.log(inputElement.value); => console ra gia tri ma nguoi dung da nhap*/
                    
                        validate (inputElement, rule);   // khi có lỗi thì sẽ thực hiện hàm validate
                        
                    }
    
                    //Xử lí mỗi khi người dùng nhập vào input
                    inputElement.oninput = function () { // oninput là 1 sevent
                        var errorElement = getParent(inputElement, options.formGroupSelector).querySelector('.form-message')
    
                        errorElement.innerText = ''; // gán bằng chuỗi rống -> xóa error message khi không có lỗi
                        getParent(inputElement, options.formGroupSelector).classList.remove('invalid'); // xóa bỏ màu đỏ 
    
                    }
                }
            });

        }); 
    }
}


// định nghĩa các rules

/** nguyên tắc của các rule: 
 * 1. Khi có lỗi => trả thông báo lỗi
 * 2. Khi hợp lệ => không trả ra gì cả (undefined)
 * */ 


Validator.isRequired = function(selector, message){
    return {
        selector: selector,
        test: function(value) {  // bat buoc nhap
            return value ? undefined : message ||"Vui lòng nhập trường này." //trim : loại bỏ tất cả dấu cách
        }
    } ; 
}


// Hàm kiểm tra xem có phải là email hay không. Search "java email regex" -> đoạn mã kiểm tra xem có phải là email hay không
Validator.isEmail = function(selector){    
    return {
        selector: selector,
        test: function(value) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            return regex.test(value) ? undefined : 'Email sai định dạng';
        }  
    } ;
}



Validator.minLength = function(selector, min, message){    
    return {
        selector: selector,
        test: function(value) {

            return value.length >= min ? undefined : message || `Mật khẩu phải dài tối thiểu ${min} ký tự `;
        }  
    } ;
}

Validator.isConfirmed = function (selector, getConfirmValue, message){
    return {
        selector: selector,
        test: function (value) { 
            return value === getConfirmValue() ? undefined : message || 'Giá trị nhập vào không chính xác';
        }
    }
}