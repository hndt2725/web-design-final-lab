<?php 
include_once "./User.php";
if(isset($_POST['id']))
{
    $id = $_POST['id'];
    User::destroy($id);
    $_SESSION['announ'] = "Delete success";
    header(header: "location:./");
} else {
    $_SESSION['announ'] = "User not found.";
    header(header: "location:./");
}