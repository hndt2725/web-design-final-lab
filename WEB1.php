<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- link font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400&display=swap" rel="stylesheet">
    <!--link icon -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title> WEB 1</title>
    <!-- CSS STYLE -->
    <link rel="stylesheet" href="WEB1-style.css">

    
</head>
<body>
    <header class="header-section"> 
        <div class="header-left">
            <div class="logo-button">LOGO</div>
        </div>
        <div class="header-right"> 
            <a href="#ABOUT" class="header-right-button">ABOUT</a>
            <a href="#TEAM" class="header-right-button"><i class="fa fa-user"></i> TEAM</a>
            <a href="#WORK" class="header-right-button"><i class="fa fa-th"></i> WORK</a>
            <a href="#PRICING" class="header-right-button"><i class="fa fa-usd"></i> PRICING</a>
            <a href="#CONTACT" class="header-right-button"><i class="fa fa-envelope"></i> CONTACT</a> 
            <a href="./login.php" target="_blank" class="header-right-button">ADMIN</a> 
        </div>
        <a href="#sidebar"  id="menu-button" class="header-menu-button" onclick="sidebaropen()"><i class="fa fa-bars"></i></a>
    </header>

    <section style="color: white;" id="sidebar">

        <a href="#HEADING" class="side-bar-button" onclick="sidebarclose()" >CLOSE &times;</a>
        <a href="#ABOUT" onclick="sidebarclose()" class="side-bar-button">ABOUT</a>
        <a href="#TEAM" onclick="sidebarclose()" class="side-bar-button"><i class="fa fa-user"></i> TEAM</a>
        <a href="#WORK" onclick="sidebarclose()" class="side-bar-button"><i class="fa fa-th"></i> WORK</a>
        <a href="#PRICING" onclick="sidebarclose()" class="side-bar-button"><i class="fa fa-usd"></i> PRICING</a>
        <a href="#CONTACT" onclick="sidebarclose()" class="side-bar-button"><i class="fa fa-envelope"></i> CONTACT</a>
        <a href="./login.php" target="_blank" class="side-bar-button">ADMIN</a>

        <script>
            
        //     var nav = document.getElementById('menu-button'),   // nếu phần tử menubtn được click vào
        //     body = document.body;

        //     nav.addEventListener('click', function(e) {
        //     body.className = body.className? '' : 'with_nav';  // thêm lớp with_nav vào nội dung. 
        //     e.preventDefault();
        // });    
        
        function sidebaropen() {
            if (sidebar.style.display === 'flex') {
                sidebar.style.display = 'none';
            } else {
                sidebar.style.display = 'flex';
            }
            }

            // Close the sidebar with the close button
            function sidebarclose() {
                sidebar.style.display = "none";
            }




        </script>

    </section>

    <div id="HEADING" class="heading"> 
    
        <div class="heading-text-1">
            Start something that matters
        </div>

        <div class="heading-text-2">
            Stop wasting valuable time with projects that just isn't you.
        </div>

        <a href="#ABOUT" class="heading-button" >Learn more and start today</a>

        
        <div class="SNS-logo-heading" style="color:#757575; ">
            <i class="fab fa-facebook " aria-hidden="true"></i>
            <i class="fab fa-instagram " aria-hidden="true"></i>
            <i class="fab fa-snapchat " aria-hidden="true"> </i>
            <i class="fab fa-pinterest " aria-hidden="true"> </i>
            <i class="fab fa-twitter" aria-hidden="true"></i>
            <i class="fab fa-linkedin" aria-hidden="true"></i>
        </div>   
        

    </div>

    <div id="ABOUT">
        <h2>ABOUT THE COMPANY</h2>
        <h3>Key features of our company</h3>

        <div class="about-the-company-content">
            <div class="about-the-company-content-1">
                <i style="font-size: 64px;" class="fa fa-desktop w3-margin-bottom w3-jumbo w3-center"></i>
                <h3>Responsive</h3>
                <p class="about-the-company-content-text" >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
            </div>
            
            <div class="about-the-company-content-2">
                <i style="font-size: 64px;" class="fa fa-heart w3-margin-bottom w3-jumbo"></i>
                <h3>Passion</h3>
                <p class="about-the-company-content-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
            </div>

            <div class="about-the-company-content-3">
                <i style="font-size: 64px;" class="fa fa-diamond"></i>
                <h3>Design</h3>
                <p class="about-the-company-content-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
            </div>
            
            <div class="about-the-company-content-4">
                <i style="font-size: 64px;" class="fa fa-cog w3-margin-bottom w3-jumbo"></i>
                <h3>Support</h3>
                <p class="about-the-company-content-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
            </div>
        </div>
        
    </div>

    <div class="We-know-design">
    <div class="We-know-design-content">
        <h2>We know design.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore.</p>
        <div class="We-know-design-button">
            <p><i class="fa fa-th">&nbsp;</i> View Our Works</a></p>
        </div>
    </div>

    <div >
        <img class="We-know-design-image" src="https://www.w3schools.com/w3images/phone_buildings.jpg">
    </div>
    </div>
    
    <div id="TEAM">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" charset= "utf-8"> </script>
        <h2 class="team-title-1">The team</h2>
        <h3 class="team-title-2" >The ones who runs this company</h3>
        
         
        <div class="container">
            <div class="card-content" style="display: none;"> 

                <div class="card">
                    <img class="card-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw8PDw8PDQ8PDxAPDw8PDw8NDw0PFREWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDQ0NFQ8PFysfFh0rKy0tNy0rLS0tLSstKy0rLSsrNy0tLTUrKysrLTAtKy0rKystNysrLS0tLSstLSs4K//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAABAgADBAYHBQj/xABDEAACAQIDBAcEBQoFBQAAAAAAAQIDEQQSIQUGMVETMkFhcYGRIlKhsQczcsHRI0JTYmNzkqKy8BQVJDRDRIKTwuH/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIEAwX/xAAgEQEBAAICAgIDAAAAAAAAAAAAAQIRAyEEEjFBFFFx/9oADAMBAAIRAxEAPwDzLAsNYNjLRbEcRrEArsCxY0SwFViWLLAsBW0Sw9gWCksCxZYlgiuwLFjQtgEsCxZYFgEsBoexGgKrAaLbCtAVtAaLGgWAqaBYsaFaASwrRY0BoCtoVosaFaAWxBrAA9yxLD2JYgSxLD2JYCuxLDtEaArsSw9iWKKrEsWNAsAlgWLLAsBXYDRZYoniqSlkdSCnxyuSUrWvwAaxLCwxFOTtGcG+SkmyywCWJYexLAVtCtFjQGgK2hbFjQLAV2A0O0BoCtoVotaFaAqaFaLGhWgEINYgHu2JYZIhAtiWGJYBbAsPYlgEyksPYlgK3EFi2wtgK7GBtTacMOtVmm1dRTtpzb7EZW0MVGhSnVlqoRbsuMn2LzZpeHp1cRWWZdJWqyStrZN9iXYkirJtkYneWtP2aUVFvtjHM/K/4Hj7Qp4lzlUqwqRcnmbyOK/+Hc91t0aGFpxfRxlWavKo0m78lyPWxmAjJZZRUk9LNJo8Lz99R0zxtzuvnOjJS0klJWvd9ZHq7M2/PDyVKs3VpO2WernBf+y7uPyNh3p2DTWLqqlBRtGbSjonljRT08XNGi4yn2PsvY9scplHhnhcHSqc1KKlFqUZK6a4NMNjXNydoOcJUJO8qazQ74Xs/R29UbM0VhW0K0WNAaAraFsWNCtAI0K0WNCtAJYVosaFaArkhGi1oRoBLEGAB7lwgQ1iAWJYaxLAAlhrEsAtiWGsSwCgsWWJYDTd8sbLpKdBaQvGb/aS1t5L5vuNh+jvZ0VOrjaqvGlH8npduT0dlz7DU99k1jaXfTpW4fpJL8Tq24OFf+BpSsm5RUknondt/eefNdYvfx5vL+Mylt3EXu8LPK+CipNpd7ta56tTF3oyrZZKUVpTlo3Nu0Y+baXmeNtCO1VKCpPDdE5vPHLUc4R7MrtaT8beZmYyvUVGrdXdKm5ystOltol4K78XHkzwuv065K0XbOKpUK9VVJqUoRjTclFvNOV51ZaX4yl8Dn21MrnLK01mllfNXN3oul0E61fD1cXUlObappPL7cl4vh4ao03bEYdK5U4unHhklxg+1Htx9PDmlsYuwMQ6OKpTXByVOXfGbt8Lp+R0lo5hhfraa/awX8yOotHs5FbQrQ7QGgK2hWixoVgVtAsOwWARitDtCsCtissZWwFIEgHtIZAQyIIMkRIKRRLEsNYlgEsGw1iWAWxLDWJYDGobv0sbipwq3WbBSyzTs4SpV4SVv436G47mNQw1Om1Z0l0bXJx0fyNYji54d9PTipypxleD4VINe1H4K3ekbBu7tehi+lrUJZqcqskpWy5sqSvbsurOxzc0vz9Ozx7jZr7bZpxsa5tHamF6DELpoPKpRnaSeV24Pv1M/F7SdCF+iq1V+yjn9ddPM1nePa+GqUZRxOEqxVvZXQqpkbd82aOh5fLqw48rLZOnmbqYSLotySks0tGr9p5u++zqWWLpwimndpJLMu09HYGModBN0Hmip2b1d29Xx7TytuYq7m3qo05St5aGsd+7HJr0aHgMK5YmioaqVdJ90YyzX9Is6O0a1ubgLJ1JpZouUVre11HX5r1NnaOx82zVVtAaHaFYQjQjLGhWBWwDtAYCNCMsYjARiNFjEaASxBiAe0gpEQ1iCIZASGRQbEsENgBYlhrEsAtiWGsSwGNjMN0sHTcpQjLSWRpOUe2N+xPhda+B4e4OPWHxWNwkfqoTdWPONmoT8uqbLY1bYGElS23UemWsqi8VJRqNfBepjkm8K9OG6zjr2za6qRsne5hbZ2TCabn6XaPEdCrQm+glZPVQesfLka9vDvLi4xcJRcfB8Tkxm30Llcbs+18fDDwVKmoxim3Zc+bNG25tOeVWt+VlbXX2Y6v42Diq9as76q/bxZ5uN69GLvKSV0rXz5pW07/ZOjixm3LzZ2xvm72FlToJz69VupJLhG6SilySSXxPSHS0Wlu7kBo9nKSwrQ7FYFbFaHYrARgYzAAjEZY0IwEYjHYrAUJCAe3YKQUGwECiWCgCEgQIgkIBCBAADN2RsmFapLEP6zCxi424vpJZHfutm8zzcTiIUoudScacFxlJpIzfoh2usVjdoyWtN0qEaaaazU4TqJtp83O/oPX2li45etle1i6LaTXFGp7Y2bOvN3Vlw7jp229nUKdGrXdRYenTg5zcryhFLlbXyV+SNf3Ix+z8dVq5KnS1aKU1SnCVNSh76UtZJOyeml1zRyzhzl07vyMLjt4Wy9y45VVrRtTS9im9HWfY3yh8zk++OIvj6zg7dFKNOMo6WlDja3Czuu6x9E7zY/oaGIxMupQpTqW95paRXe3ZeZ8w1XKc5Sk7ylJyk+cm7t+p2Y4TGacWedzu22bL30WVRxMJOS0dSmotS73HSz8PgexR3lwU/wDmUO6pGdP4tWOdRjqOqY0xt1ChjKVT6urTqfYnGXyZbJHKXRT4oyKWLxFPqVqsbdiqTy+l7E0bdLYrNK2fvJXhUgq0+lp6Kd4xTS5ppLU3S99VqnwJYoMDCwMBGKx2IwEYjLJCMBSBsQD3UhkBDICWDYiGSAAQ2IBCENL3x3hqwqrD4ebp5LOrONszk1dRT7Ela/j3BGzbR2vh8P8AW1YxfFQ6034RWpq20d9pu8cNTUF+kq+1LyitF5t+Bqlm222227yk3dyfNvtZHbga0HxuKq1nnq1JVHzk727kuC8joH0HTcNp2zWUsJWi436zz05LTttlZz3ikkbj9E9Vw2rhnxzOpD+KnIo7dvpsmO0KMMA5unDETzznHVxjTs4tf9+V6+6cP3CwVX/OMNHp3RlQnUblTXXcLqULPjGSun3M+gto4iGHpVsVLhQoVKrb92EZTfyPnz6P6k/812e5SvKpVkpy96UqM2/WVijof03bSVLZ9PDx0liqqb/d0rSf83RnC4tcXozpH02Y/pcfGgndYajCL7qk/bl/K4ehzt0ota6d/IUUTqJPnzsXU2pJNdph14ZXbi+CMyjokuSIh8oJIdFM5628wrGnL2mdA3YxfS4aF3d026b8F1fg16HO5cTadxsV7VWl70VUj4xaT/qXoSkbcwMIGZUrFGYoCMVjsVgKQhAPeQURBQBQyAhkBCBIADkOIqOpVnUernJz/ik2ddqdWXg/kcdovq/ZXwLEW2KZUs0l2aF4aa1RoNDD2V27vvPb3Or9DjsLPhlxFK75JzSfwbPKepfhLxkpLitV4rVAfQv0iVsmyMe/eodH5VJxh8pHFvo8Uf8AONnZrW6aq9eF1hqrXxsdU+kjFZ9hVai/5IYKflLE0X8jiWy8Y6OJpVY9aDq5e5yoVIp+TlfyKG3oxzxWMxNe9+lrVJx+w5PKv4bI8erSfEzZx9ox9o1FGKiutPTTl2/gQYtCjnlFRyzqTmoRi2lq2rXbskm3xv6cS3F4qDk5Qp0qMU1Do6dV1Hw4tuTzcrrTnrq8F03Z2Xrrcq6IiPS6VNXRjOV3Lw+8qpvsJB6y8igM9fdOo44ul+vni/DI380jyWZWyK2SvSkvzasG/suVpfBsg6YKNIW5lorFYzFYAYrCwMAEBcgGwIIEFAFDICCgCQgQBJXTXNNHGI6W/vU7QjjmLpZatWMfzKtSDT0vlm19xYixDIqhItiaGdRtlTLKaMTDz1tz4eJlxVgOobcxsa27VJSnGLlDC0c0lUcc1KvrfJFu9qfI5fCjBP2anSNJ3tBwivBt3fbxSPexmMb2LCl+j2jbylSqT+cjXsJwb70iiOOp4k6iqVXLjFPLH7K/E9TatbJTdutP2Y+fF+h5NFZbeBBdUa8CiaLIxzOWtlGLk3+BTCV7vk7AV3BRerJIWD1ZEWIEXaXLv5FkIlU+IHVKNTPCE/ejGXqrhZgbv1+kwtF8oZH4weX7jObMtIxWEVgKwNkYrYEAS5ANiSGQsOCGIGQQIKKCQhAIcj2/SaxeJt+nq/GbZ1w5bvQksbiV+0T9YRf3liPKg328SyFZartWpSnr/egeiUnd38u00MtSs7ritUZ8J5rPmeaZWCd3l5aoD1qmJ/0dajb/AKjD1k+TyVIP7jCodRd7YmMnZNe8o/CS/ErxmI6Kjf8AOslH7T/u4HmbSrdJVstY0/ZXe+3++4WStYpw0S2q9UAJuy48VZ+AHFKyXDj4kqcEBsCqfEqg/aLahSusRGVBldQeItRAbnuVVvh5x9yq7eDin87nvNmq7jVP9xD93JfzJ/cbQzNagiMNwNgKxWMxWAAi3CBsUOC8B0LDgvBDIgYIEFFBCAIEOW73xvj8SuzNTv8A+KB1I5ZvZL/X4l/rQ+FKCLEeTJRjZdo0HqVNpu/b2FkGaFqY8J5Wpcn8O0rTGAytoy/Jpr3ou/c2YG162arkXVpq3jLtL3V/JOL/ADXFrvWZHmu71fGTcn5sC+ggVeI0HZFU2A0noVtjX0XoJIiJUKPzi6oUp+0BlU5dwtQN2KwPe3Ln+XnHnSfqpR/Fm4s0bdGTWLXKVOovk/uN4ZmtRGIwsUANgbIxWBAiXIBtEOC8BhYcF4DkBQQIJQSEIBDlG87U8biddFVa07Wkk/ijq5xnHVM1atJe1mrVZX7NZt/eWIrlppHR8yQAoS7bEgaFsWWIxoKWbXhz4l6AWouJidplVb2bv2MxnxCLFwEYU9AMKlV5oxil1b6+LuIyxCSIhJv5GPa7LZsSl1gMmnG3P5olSy4jq/YUziB7G6kv9VFfqT/pN3ZpG58H/im/dpTfxivvN1M1qIKwtgZArFYWI2BCC3IUbXDgvAdFcOC8ByBkFCoJQxAEATEzywnL3YSfomzjNJ6K+rsjru2qyhhcRNq6jQqO3P2Hocjje2hYiSTfHRFdL72PJWTuxKa0RoX3ImKFASo9H4MxI8I+CMmfB+DMSj1V3MItQCIgURJDSK6j4ERXMFFa3JIto8PUC++hVOQXcSbA2fcqk7Vqj/VgvK7fzibMYOxcH0NCnDi7ZpNdspav8PIzWZrSMRsLFZAJFbGbEZRLkFCBtsOC8BkQhAUEJAIQhCjzd5f9liv3M/kcpiQhYiupwYYcF4IhDQsIAgEnwfgzEo9XzIQiHIiEAlQSr1vT5IhAKpF1DqkIA67CrEdvgQhR0jC/V0/3cP6UWMhDzaKLIhAK2KQhQCEIB//Z" >
                    <div class="card-info">
                        <h2>Sơn Tùng</h2>
                        <p class="member-position">CEO & Founder</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>

                <div class="card">
                    <img class="card-image" src="https://danau.vn/wp-content/uploads/2021/05/KHG1614.jpg" >
                    <div class="card-info">
                        <h2>Anja Doe</h2>
                        <p class="member-position">Art Director</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://lavenderstudio.com.vn/wp-content/uploads/2019/10/35010952605_e06e67a2ac_o-1024x682.jpg" >
                    <div class="card-info">
                        <h2>Mike Ross</h2>
                        <p class="member-position">Web Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div> 
                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRJiLtl6vn77IKPJvHuE1zGJ4Fm7NldSBL-w&usqp=CAU" >
                    <div class="card-info">
                        <h2>Dan Star</h2>
                        <p class="member-position">Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://bloganchoi.com/wp-content/uploads/2020/08/rose-anh-bia-1.jpg" >
                    <div class="card-info">
                        <h2>John Doe</h2>
                        <p class="member-position">CEO & Founder</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>

                <div class="card">
                    <img class="card-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRUWFRYYGRgaGBwaGBoYGhgYGRgaGRgZGRgcHBgcIS4lHCErHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHzQrJCs3NTQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQxNP/AABEIAL0BCwMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgEHAAj/xAA9EAACAQIEAwUHAgQGAQUAAAABAhEAAwQSITEFQVEiYXGBkQYTMlKhsfAHwUJi0eEUFYKiwvFyIyQ0Q3P/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAkEQACAgIDAAICAwEAAAAAAAAAAQIRITEDEkEEYSJRBXGRFP/aAAwDAQACEQMRAD8A3N66iBSVGw5DpXcO6ufhAHgKhiMsLPyj7VDDXCDoNKQhsMMsfCvoKqNtR/CPQVO3ekVRdu6wKYEDbBbYegqx7ax8I9BXRbMSa4DSBFliwvyj0FWvbUfwr6Ch1u8qsD6a1LRaZW6LPwj0FTCL8o9BUDvU1qBtHwRflHoKuS0vyj0FVZq4LtUmKi10X5V9BUFsg/wj0FUPcq+3dpphouS0vyr6Cpsi/KvoKirVXfuQKBbOsVn4V9BXMy/KvoKXPcJMileL4nmJAMINzzY9BQm2NxHWJ4giDRQx7gKR4n2sRGjIB10B+1BX8cuQkkKPv57/AJ5VmMTbaQ8ZVJ7I5nX18quhUajE+0t2M6CyyTABQ/WSDVuA9trLHLethD8ygOnmIkfWs/fxKsuSWVwYOsqeR7PLWlHFsAURX2zbbQfPadNjToGkevYZ0dQ6ZGU6grBB8xRK2R8q+grxP2e9pbmFuSpzIT20OzDqOjd485Fe08OxyX7aXbZlHEjr3gjkQZBHUUkiGiz3S/KPQVH3K/KPQVca6q1QioWF+Uegrpsr8q+gq0muE0ADNYX5R6CuGwvyj0FWMa+JoAGa2vyj0FDXcMDrlHoKO0qLRSAFt4YRsPQUDikXMdBy5DoKcBqXYsdo+X2FFABYS01xwW+EAQPIU0vgDSruH4cBR4D7V3F2J0FSkAvuO2yVZgVKmX3o6xhgo61y5ZmmkB25eB0FU32gUC14K8URcvAigDtl9TNV38VGk1KyhImhcVhmOtSy47CbWIFEpcmkKMQYprhxWTwa0GTUZrvKh7rU7FRNtanaWKostNEPoKf2Sy73lCYl5NVNiapu3KlyBID9ocdkQKpgvPko39SRWTv4qFHfr5UR7Q4rNcyzooA/c/es5j8Toe/Qfv8AWtI6GcxXESzb6DQfuala4wR2t4BCTrvGvdrz6Uje5yHPSou/IeA/c/WrTA03BMUbjuSNyI5dyzHcWJ74ojiN9mDKfhzQR6a/UVz2KwsoCebFvIaCjcdhe3dWP4Fbzkj/AICps0UVRkeJ8NZNRtWt/Szj+S42Gc9m5Lp3OolgPFRP+g9aKxOBV7ILQAUBk+Fef+9azeV0OttwykaSVMx4f1NCkTKJ+iPe1NXpRg8YtxEdT2XVWHgwBH3ok4iKqznDHeq2uChExOY0YlmnYEPe13PNSNmpKlAEQlVXEoia5kmhoAENSzFMcx8vsKcPZ1oHE2+0fL7CpoC/DYnQDuH2opCTrWcwSvodxGvQU1t44RFJSxkoZsCaquNA3qC4xYofE3M1UScvYdSJoW2gBrl/EECgP8QxBpWA/tXVobH4pVB1pPaxBEyaAx+KkilJ4NILI1wyF9eVMLbZaAweKAUa0Ne4gCdNqyksGids0K3qHxFyllvGVK7fmpsqhhhroq69eEUmtPVz3TSciHHJF7mtV3sRFUXWoDiblUdj0+9TF2y+uDNYzE5mY6yST66fvTq17JC4iO90MCUYFCGRkJU3EJHaRwC2sx51l8PeOZW/mB2B+HXYgj1Br0bh3tNgHhRdCuSidq3kd3uHKq5VUZoMSw7IzDU105p1szkYD2w4KmGu2/dElWQmCSTKQpYk/MxOgGmXvpAi9o/yqfWP6k1qPbXHq91U+L3QJDgyjpdVLtsqJMNDqG65BHQKOG4aQTzJUDzcT9KHJ1kqMbNV7O4C8tgPmW2uWAW1Mde7rRmB4fnNxvfFy0KG3AAmdPM+lHYrg5vqiszKijQL5a/SKPw/DFQLBPYQW11gBFAAGUactzrU5aNMJlHFsiW/hLhVJCqMxbKNABzNYL2gt3XtKTbiczMqowCFXhZc6NmWTsIr0i9h5Gm67VWEDpDDTYg06rIrvAo9h8UXwlsb5MyeQJj6EU8ZHO1IPZBBZvYvDfLcDL/4uJHoAK3lm0CNaaVnLJVJirhaMGOanqtpQhw+sijraaVaVCKTcqHvDRLWxXy2qYFKCryK6ViuUWBzLSrGDtny+wpizEUsxR7R8vsKAJKoS0oA/hH2pR7gk9/OtHatKUUHoPtVN7D9oQKlxTAVXkKrNGYNQVruKAIIFA2c66Dal6BTxK6FcDrVD7GKncwxdpNMrODEa1QGeFtjM6UuxFsqwkzrWjxiZToKzOOuZm86xk7dG0Fiw1DpUbdkkwBVNq5pTPB3ViqpE27Ii2V3qeeuYlwTANSw9ktWUvo1imEWYqT1YtggUPe0rJsGmQQS2tJ/a65FsIN2J9B/2KYm9BrJe0+OzuR8qgeZ1P3FVxZZT0JL9wKGj5Y9fwUmZ6NxT9kjv+wn+tAKJIA7vOuwybDcOhCADc/0n88K2fBMIC2HEfxAn/SrEfULWVwF0K6lllY1A6EH+tbvhVxC9rIZXMwHI/AxAI5HSsJvJtCqNzbTSo3U0qyzsKndGlCYvRYLipAPTUnrX2HvB57Ma0lxnFrksP8ADXB8oJVZ8WXMfIepqfAfeIly5iDlUywWICIoJMTqRHM9KGzd8XWPZiz2fu5uM4pOWQDzVLX7zXpiIBXjn6a4g3uI3bxkFxdeDyDMIB8AQPKvY3nSto6OCWy0KK+iuLVg2qiCAFcZ6qv3gKFs3CT3UgCrj1G081ICasUAUwOPoKQ4zEjOfL7CnV1pFJsVhhmPl9hQwGSAlAdtB9qsR5FQW5Kr3AfarEZdIpAL7mHIckV1Y6UwzqTUL6iI60wF1xBNXLtVq4QAa1y4gHZG5pAKcbbzaDflVFv2ZkqWOnMU6TCZWDMdqtxGKXLII0qUltmlvSMV7TcOFkgpoOdJLN5p3pxxy/cvMVCkqDyoLD8HunkB4mok7eDSMaWQnCtrWjwEVmXwrW96b8NxO1ZyNEaE2wRS7F4Wj7NyRX11ZqWk0CMxiLETXm/EcTLMerFvITH3+1eqcaGS07dFP2rxzFOTJ7vvyH0quFZYSwga6/ZGusk/n1qOGHaJ6D67D87qg+o8dKkjxm/N9f2rpMPQu3c7Q760vDsWUKPuAwMjfoZHgTWQtMSygVrMBaKgEjQ6Gs5I0gz1Dh2LzKCDI5EUeLoNYf2ZxBRzbk5SJWfzpWuL6VjbRq0mXskzFYv9R+IizhjaDdu8QoE6hAZdiOhAy/66S/qFxm/bvolq86LkJIRisktHLwNYEXndy7uztzZ2LMY2ljqa1irVmcpeG9/ShYxR/wDzYeuU/wDGvaESvG/0qH/u4/kYnv0/rXtS7VpEwlsreoTXXaupTEAXrLE1cLeUUWIr5hQkKipNqGvXYNX4hoFDIM2tMQvxHEsr5aFxOOJY6Hl9hTq5gkJ2oHFWQGOnT7CpSYEWdsoj5R9qpwrsFg78ool1zBEA5CT5UacKFjSf2FKvQAsJmnUU0a3pJqZgAECu771YEMR8IiltjBMXzljI5U1dxUoBGlFAL71suCCSKzuOtZHABJ8TWseyOZrNtgA2IMmVI0nxqJLBpGbiCLiEXciu/wCb2xzp8eHW1UrlEffzrC8YsKlwhBp3DSocWslKfbATj8arns7VzDXIIpahom0ahmyRpcNitqPR5pLgUpvaSoKEftnfy4Zx8xC+R3rx/F3J/pyAEV6X+o2Iyoi9cx+w+xb0ry64dvzvJ+v0rTjRE2V3B2R4+m355VHPofH8+9fXWk5R1+lF8OwJu5gNBJk8oH71sZwg5yqOxj7L8LNxsxGnKtnicMqIZgaHfQTy18azuGZkARW+FZYCdQN9B3a1fb4iqbAZt8/ukY9YO2+0zUtWd/8AwcsY26HXC2JuZ9cgEKds07kd21ay3fBG9YpOMaAtkMx8Eo2sT2HOoE8iaMw/FkYwjgMDGVpUzExr3VnKDMXCUcNGX/UW5OLK/KiA+eZv+VZNDBp77ZOWxbtzZVP+2P8AjWfLVpFfijmldnpf6RGcU3dbb1mP3Neyqa8f/RZA13ENzRFHk5bX/YK9fmrRlLZFlr5jpUbl4CoI+agkttDSuu8V9deBS69fJ2oboolefMYFctW2WqsChLTyo2/eGw3oX7JKb2Ky0qxWM7R06fYU1GHkSaU4x1zny+wpPYDzCqMq/wDiPtVt25oRSzD4ogjplH2q7B40OH0+ExTuyjquQABV6YgSBQFm05Z22XYD966OGMxBLEQZ0pZ8FYTeEvpuaLzZBXyIAO+lOKxDhwTsOVPQgu52iJoPHoVGdASZBj6VevEVMCKuVwTRhgLrXEgSQ4ynvIqd3D2rgIgGqeN8PFwLl3DD050UmGCKIFH9gYjimECXCq7VC1R3HbcPPKl6GsJLJ1RlhDjDXgKOTGCkls0ThhrUNFWY79RcZmuqvRB9Saw915+w/f61pvbaRiXkcljwywI/OdZV0kx9fvW0FgiTycRSSI3Og89K2nCbQRAkASCSDPaEaR4n+vSsjhVm4iidWAEb6kAfU/StycoVoJ1lZIjtLHPwM8oOvfTkep/Fcdycv6RRcQLrAiNdhIgx4giO6I8agbh0PUQCYmPQaVe96UYsQSAsR8Q00EHYQNx1WgLzjKN9tO8SeZgjWdKD3l1WxoMYG0ZVIB2yqR3SsRMneuY3hqOA1kQ4/gJG66lVIJiNdDpqY6Ul98NQPPSfDU1Zb4kwMADlJ7TDQSOcfenZyc8eOStIGxFrNDEEsBkzGZEde/eelZ28kOw6E1r3c3EzzLEw23a5zpz09BWc4paOdmjn2vM70I8j5nElFOJqv0o4h7vGhJ0u22TxKjOPorete3XHOkV+e/YH/wCfhuof17JB+knyr9CkjamjyJLJApmFE4ZIFUs8RVj3gq00SUcQcxpQuFcEa71bfuSJoLDWZeRUt/kOhzbgDSoCwJmrLVuK+DCdasR9NIsdhUzt5fYU5uXgBSfFDtHy+wqWUW4O4rohYZQI1PhVz4u3bJVVJJGsAmvuG3RcUAL2AAJIiYEaUVfyAECJoJKvflkVgIB9arbFMpYgyOlSxN7KiREEjTuoJMSrl0XlzpSbWtjX2V3ONCHKgmNDpoDSTD8fzISwJJPQ7TTrF8Rs4dIcbnYCSSaAxGIt3wq2l7UjQjLp31LV4sV0MuG2c8tljQU0tYKGPSKjZcqg0Eio/wCbpsDrVpJYAtvDKJrjXAyUnbjWdnQ6RoO+q1vAoRJEUdkBZ/kYdi7tm00B2FIcXwt0YwOzOnhWmwhGQHOSKrxZZ9ApjrUSqrNYN3RnreHNMcHhauRBtRtggVhdnRRi/wBQ+AB7LYhRDIFLH+TNDmO4EH/TXk12ATrP761+i+JIHtOvzIy+oIr8+8RwLI0NIOo79NNfMVtxmckT9m7WbFWswkSWMcgis06dInyr0DGYD4SsZYiBoxMkMAp5jX61lf0/sD/EM7bLbYCerwvoM31Fb/HsAkht20gSYytABmSSevfHdbPV/jpOEbXrMs7BCNCSDDIdARy12O5FLLxhtTJiR++g8adXEDDtDvhZJOxMb6drnHLoZV8RSFJBGnIEyIAXnuAdjP3go9aUtsUXr3nPM7z4V9YaDJPhoTt3zpQ0nkNPznV1rQnnMH8ig4FLsxi16UcqdRlbTfRgD9/pQ+Kt50DqBJ0PjsQfvRNlZDAxqra6EE5YI3nnQfDGJVx4HvgyDH09KonlqT6v1BvsNhWOOsKm6nOx1gKvxT4/D517uWryz2E4gtm8UOWLoCho1DiSNejD6gV6YHCrJpaPE5+JwlTB8RiCGXxo8pnABpO13MwI11p9gW01ojkylHqA4lCBAFWcJsMAc3XSi79xdq7Yaisk2W3Tlrnu51qvEKWr4MctUMpvooMzQGKIzHy+wou5aBiTQmJjMfL7CgQVZxQKIVMLlH1FDtgpbOWmP3pYnEkCJO2ijoW2phgHZwVaNDrHSotNhoA41clgrNC6AZZ0J6x5UIMKUJKuZj1p/huHWtW3Jka66zSa/ZCXSlzQt8EchzpNNKxeluAVMTb7XaNt4M9R/wBiu4p1t3lVF1I1jp1onhpRHFpCCQCz9defmapxeHZWd9ik5SeYOtOLpZAH4liXRHjMTyjvpPexKgqdQ0SaeYDFM4CMAWbfuFSxuBts6owhsusdKmWcoQm4VhGvOWVo/ejfcuGZH0PLoRUr2CyXUTDvDRLDu6mtC1hnC50ErzqlGxgHCUVAAwpyjKRQWJW2QYgMOW1KcZiytskSdOVPWxoPxWDlwQdO6qcZhHQZl1FZPC8fduzmMCd6dn2hlMpiahuLN0poLw1zMKR+0vskuJlk0bLHIDu5b6mjMLxBQd6a4fGg86yTo1asw3srwFsIjjEfExUqgggSTMkjQnIvp30x4uyoFYwFJPZmY+L+HbUSNdBtTbjkOpGmZnGXvCqDHdsx/wC6wvtRjSDpqVgZo8APHtb/APgK3Twet8aCjBP9XZcQrK7RIGaCcxCnt/DAljIYjxFJHbMGE6mZIG/Lf5Z+p51d7ONmS+jN2QubtGVnYz15ERsfGgr98Zzl5mRA0A5adB07qDpXL2jbFbeuvX9qstN18K7iWlifPvM1Sjwdh9aDkvrIaYO/GhEDmQIgde+dqBwxKuegJB5GNa6rSNeUzqZIMaT5VViN5BmQCY6ka/WgJy0xvbxADCDrKss8ipzT3QYPrXrd/iCtYVx/EgYDxAMV4ktwwJgjv10Ejx5mvRuBX/eYa0x5KVPijFf2pTdROL5n5JM1XDWBQQNQKY287aa6Ut4BhWKk8p0p1YvBM070QVpHnydYIXyFiTtUMLxNWYhTMb0FdU3nMmFG469fKppZVZCiD1iq0yNjC7ioMg+NQ/zBSra660BhOGtnzMxPdRCcOVXnzijIjmHvO05hHTwqnEv2j5fYUcW+InQcqWX1OYyen2oyUZC1ZuBVzuAk6gjqdD9aJ4dx25Zue6RQ+Zt2O/KZq7j+GtlbaBiMiL2v4HZj15xsfKsql5rGJJPaCztzBEGPpWTSTwK2einG31QOQufN2lnQeBjWlnGeI5wjswDiYA1FdxV1sRbV7B7RUg66DpNYjE4bEKdbblgSSVBI01PjR+S2N0el+zJtsnvs3bIIcnQ6Hbyq7H8YsNbYK4d2lQqkE8942rzDG8axNtMjIbauJ037yI2qOAuJbh5dWIkaHK3ntVd8Co9G9l+HOjM79I8hR/GsWqo11ACUB16901g+F+2F1XYFcycxz8a1+Gx6YlHRE0ZZKmJpqqpaDRkP84xPv1xPuzEZTEwVmtaPapcwKtoV2I50G+OS1ayus8oAk1hb+OyswA0JJHIjuodx0I13FuIlwbs5T0B6Uvw3tCNVIkRSV8c2Qg6g0FgbZdwJiTUyfprGOQx7x947KNzsKMsOee9MMFgBZebkEEaVdxXDKe2kRSq1aNe2aAFc024c7aULg8NOpp1hAEIaJjbx2E+dHWy4ZkooH46wfKqsQRmgA76gAMPX151537T4nNckCILDzBM69YI7q2NhicQp7Q7BZgSez69wkchrXnHFr3aOuo0PeeevPpNWe1y1x8TQfwa/ltXnOoJVdyBpJMxr8v08KVl9TuZ9fWjsuTDIvNiXOvzbaTr2QpjvNLNfDT8FBz9moRX1f+l95pjaABsDH13PfVcCajdOuv8AcePfXA2g3H0pkOVvIXh9CSATGomeXWNjXcRbIG2gMjwPX6VRaI5+POfzeiGcMrAAzEiY2GpH0oKv8QYnsjlv56xrXo3sFYL2FAOzt+xrzPNIH5sa9V/Si4DYuDmLp9GRD95olG0cXyJfjj6N/wAPtFEyigL7ZWbMY6zTe1cEE9Omp9KzvGbmd1KMZBLaAgzplQhjtHaIjfLpVZUcHnt28ny2nR8yuMrFYDRoApGVfEw3Pb0cW74C9oaz9I3nxpBbxrOSpMuhUMsDssRMHvgg+flRd13JAIA1OsyCAu2g3mNKabd2idDLh+L94WI0UaeNDcQxQQFi3hULF1MuVIkrJCkTzjQbT31muK4a579Ae0syABnEiTBUgzp3VEm0h2jU8LxHvFzEGD1EQQeRJ1oLiPE0W4ylSYjZXPId1fe/bLkK5QQDsBlBIgELtJjaee1B4m4udok6xpm3GhHkZHlS7y8Q1XpTgOE3nCC4UyKBse0EZWjcQYOUGDOtT9rcAES5dS2pQIqhmIVVCAARO5MwNyTTbE4ootpAnZ3cCF7CKSSoHVsoAJ195SXinDcPxBmtB3tZIZIIOdHRDnKNqD2gAZ2PlRWWgtCX2F4ZfuB7lp8iSyagkTlBErzWYEjUV6LhMCFRVLhmRQHaIloGY9w3MVXwvCYdLYs2iFRc0gEr8Rluc7nrWc9qOMpZUI7MuV5zoSHKEaDMIMyPvTTSVrQP72VcWwFi7ibGi3AxgKW7IEElh36fSjuK8LS6Pd2gnZ3B0AEfwmN682s22d1dLxCK2ZS09keFNxx7EW+ynbLMzaAkkRuO6pTisP0bTeiniBWzcCMkOvTUGKe+zHE0Ju//AFmOy22+49ayPCrdzF3bhzQoPaJE5JNOcZCWzaBUumhIEBhStKQqNJhb6apeXNrIYa77eFLfanAYdELWiCx3E6jvpT7KcNu4h2zuwt8oPQ044/wdMOjOGLCNZ1I0q1bQjLWbIKKS2k60bf4eqrmQ8gdKV4SxnUkNodhNWYK3efsKeztPQTU0mXGTTCL2KulFL5yvInb1ozAXiYBJitnfw6nDZGysMm/fH9a89R8kidqTVG0ZdjUJeAFSuYplQMBILhTPMQWP2nyrI3Ma86VoWYjD2xpMFyWkamAR6Mw86Fs7/g8fblv9C27iCr3XKhlCGeRjVSANtZk1hscc7GNWZhv1Y/3rT3lCi5MagjukMC+p+LYD1rPYXtXQYGjFyORjYR4mqTO75uUkvcBXEyAwUfCgyjrC6dBG1Lc3a0+tX4y7mZo01O5nc+JqhDHLnP53b6d9NHLySt40fMvp+aVEMZ31P0qy6891DtQZydPBas/1oq3cB8x6bzHkT9aED/f1qWb8/O/70DUsYI3BGmnTTu0/avQv0sxWU4lJ3yMBuRAcExz3HpXnl+QV8Na2v6Z3R7y+pKgZAzSuZmAMbnRQJnqZ7qZy8rw0envii4YLv3dw5eelJbuKdLU3RDCcxBGaWOaTyJkkQpjXQmiA4hspCsJEMYy7aMVOhmdduyaqv4fOmf3mXKGOdSMkwRmImY5xIiaTbUc4f0cVKwyzc2JYFoEEiGI7wSSYga+FdB1zaGTpLAnKd8qiAqjluT3Uj4YMrO5zF2IUt2ipBPYMHQA+E70dcsOZ94O0RAZTAgMDHXuP4ad07bFQReuhSzE6bkyNBPOToB31PBX8xYyrZWKsRIIYdQRpoR4zXGsAGMuZYBVdTtG/fOtTcoqlh8QUgCdzEgHu1GtFuuzrAq8RS+JIeBsIBCAiBAiWHTpppyruIuIGOi+lcw6OFLgwSQUB00iDmiZ68tgNKGuu0nLljz8+fWpg5Jdn74N0hjxHiN1XtpbthrZCZ7jERETCjdj2T6iirxRJdmCBspYsAADMSX5zIrzMYi9buC4LzyTn5AaHKBHh/wBUy4vxy7cb3ZMKCAAO7cnqSQDNDb8GH8e9pTh0DWGCPnOcv2kYawhQiWBjNI2BGutYu7iLmIV7lxcxMkwBkVmJIOnLwruPwpcsS0wc3aEzMiDBGlfNaDG0zqhXmgQII2gFCCPrUq6wAu4figjDPsdCswdDsDWnWwXe3kMKZUydUUwZMHSh+NYdET/0kC55zZu31+GfhOm9BYK0dQWJlRrsZg699KSeyk7Q+fhV7DG5/hTcuF1lzlWBvMEGazKcXJVluEq8wSRFNeHcWxVhYS9Mkg5kB0B5a6UBxxPfvmIVTGuVdz1OtaUicl2G4+UTJbLKBz69da068QV7CIXzSRmDmT1OvOsHbwBj4/8Ab/enD4PJkymNOYn96Eqdg0aP2rs28ls21VQCNhE0Hh7iIFY9mlGMxl1wLLOCoIIOXtDzmpMjEAFvp/ellDSscYviQcEKTqI6UJbsiNhVa25QHY1BAetRK2bwpIO/wqQSeVd4/dKrDTsFUHYdlWJHXWPwVVhRJAbUHQjbfTevuOrmCEnYH0BywOmnjRFNbPY/j3GMXIQXXzWrrHdYGvysSIkb7n130pPhWIZSND2vEQAQR6U9bBf+m3a3YA6bgagb0ut4GOanUbg9CDsw3n6VaJ+TyJtMWM+p5zuedcWdfp3edNXwGpEr45W+WPn69rx7tK4uAHLKJnk2kxEdvly+s0zkc1Yrfrz5/n5vVZXrTd+HTpIB65TOgM/xc+fgPOtuF/z/AO3+9AnNC0Hpv9q+R6YDhf8AP/t/vVlrhf8AP/t/vVAp5Fl4dkNzn6f9099hcZkvuJAzIo3g9m4jkDlqFI666dwl/heh7e/8vf41XhOHlMxD6lSB2dBsNp10nzg8qDm5mm8HpljE20DFciqobMxcljrPxSxdpOstJJE70RgylxJbIQygm3KnfVVgGI38TNYLH4q9c0a4Pd6RbCBUBAGsKRJmTrO9a3g+FHuQ7BS0EkquUEjYkEnWGIpU7T/RzUOuHoFlcpAIKlTAgT2fhMaALER60YuIlm2aJADESsmQD3nQx4UvxGKVDItjaBrG1snXTXaKQXuIF7lyAFmDpGoVVgNp2teehgkdIUou7bBGiu44BSSg0GxhhpqBtI1A67UPw7EPeDB1VSseJBHZOUAZevPflFD4DGMAib6DtHc7Ez1matNzICoAgrl07OwBJka6z10il1xTeB6eC/F45LCZkCO2i6mDowVpMH4cxMd0ab0tPGAe7QaACBpy7NALhBlQyZdu10MuQNO4R4xrQ9zMWaCgGYgDIDABIGs67UpOkkgSvZ//2Q==" >
                    <div class="card-info">
                        <h2>Anja Doe</h2>
                        <p class="member-position">Art Director</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjwIxN_nXjeMn7MVGZVK7OsLFAn6-5t6x2GQ&usqp=CAU" >
                    <div class="card-info">
                        <h2>Mike Ross</h2>
                        <p class="member-position">Web Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div> 
                <div class="card">
                    <img class="card-image" src="https://phunuvietnam.mediacdn.vn/thumb_w/700/179072216278405120/2023/3/8/3350930552366233987313177529641311612280868n-16782884601191831530983-614-0-1289-1080-crop-1678289531368132564451.jpeg" >
                    <div class="card-info">
                        <h2>Dan Star</h2>
                        <p class="member-position">Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTllQWNGY5jvDB1m6VJUbdXWBKUCq-d7lyrGA&usqp=CAU" >
                    <div class="card-info">
                        <h2>John Doe</h2>
                        <p class="member-position">CEO & Founder</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>

                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjladW6wE4Ky6obz061AbMsWN75T5-_h4IJw&usqp=CAU" >
                    <div class="card-info">
                        <h2>Anja Doe</h2>
                        <p class="member-position">Art Director</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw0PDw0ODQ0QDQ8ODw8NDQ8NDQ0PFRUWFhURFRUYHSggGBolGxYXITEhJSkrLi4uFyAzODMsNygtLisBCgoKDg0OFhAQGi0dHR0tLS0tLS0tLSstKy03LS0tLS0rLS0rLS0rLS0tLi0tLSstLS0tLSsrLS0tLS0tLS0rLf/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAEBAAIDAQEAAAAAAAAAAAAAAQIGBAUHAwj/xAA/EAACAgEBBQQIAQoFBQAAAAAAAQIDEQQFBhIhMUFRYXEHEyIygZGhsRQjM0JDUmJygsHwFSSi0eFjc5LC8f/EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EACERAQEAAgICAgMBAAAAAAAAAAABAgMRIRIxQVEEFCIT/9oADAMBAAIRAxEAPwD1NFIUlmpSFQSqAAAAoAAAACgACgAUqRCUwMHE2ntXT6WHHqLoUw6ZnJLLNN236VNDTFrTf5y1NLhXHVW/5+F/RMDfcFweSQ9M/tRzoPZ6SUb+PHipcKz5NI7/AGJ6Vdn6iShdx6OT6St9qlvu410+KS8SThveASmyM4qUJRnCSypRalFrvTRkBiCgCAoAAAAAABSFAAAAAAABQOKikKFVKQBLIBAAUhQAKAABQBUiI4e2tp16PT26m14rrjxPCy5PsivFsJZ7V2nRpKZ36m2NNMOspd/ZFJc232Jc2eL76elS/UOVWgc9Np1y9cpSr1Fq7enuL6+Rqm9+9Oq2ndx6ixeqhKSppr5VVrvSzzlj9J/Y6H6BPD62XSnJzm3Ob6ym3KT+L5keew+aku/5maljt+QSkoeb+eQn3t/Ern/ayYcXhkgbFuvvjrtmtKi7NDlxOixKdEn2tLrHP7rXxPbt3d+tLrKI259VNSjC2uT/ADc34/svsfhjqfm9PHfjufYczZ20Z0ObjhqScZprKcX2PPLtYRY/VdV8ZPCfPCl5p9p9TwvcLfyyF+mo1EnKpZgpv2pxg02od7SeH3rB7hRdGcVKElKLSaaeU0+0lDMFIAAAAAoAAAAAAAKAAAHERSFCqlIUJVFIigCkKBQAAKABUeKembeh23/4fXhU0NStk1mU7WspeCSfZ14n2dfYNra6Om0+o1EucaabLWu/gi3j6H5X2hqJ3WWW2ScrLLJTnJ9ZTk25P5hMcVzObs/Ztt/OK9n9pr7Hw0OldtkYd7PXdn7HjVVCKj+ijLZn4+m+vX5e3ntW7DfvSfwR2FG7Va6xz5m5x0az0OVVoV3HPdtdOOnFrGk3dqS/NpvxOzo3Y07/AFMcmxV6VI51GnWDO7K1mufTT9TuhS1+bXlzweebzbElpLOX5t8128Phk93lT8Ua/vPsOOoqnHC4uF4fiX17bL2z2aZZ08Wpsw1LmsY93qmeu+ireSHDLTqy6Vi9v/MTU1KHdFZcks56Z97yx5FdU65zhJYcW0bBuFGUtfpq4SjGc5vglNNxjLhk8PHPD6fHwO159j9Kae5TipJNeDWGfQ+WlhwwXs8Dxlx5PDfXofYIQpASKACAABIAAAUAAAAOGioxRkFVRkYooSyKYmQBFIUCoAoAIBEJdJvxQ7NmbQhHq9LZ9Fl/RH5kfP5H601FKshOuXuzhKEvKSw/uflW6hwttrl1rsnB+PC8fLkEx2W52k49VTHGW5fBJHsk6OFY+hpPo42O1bXdJfqZT8uJrH9TaN5ttR0y5R47MZ4V0j5/7HNs7rr1/wAzthKHPociqvK6GkLfaWVxaezGebzhfA2rYW36dQkk5Rl3Si0/9jLLXY3x2Y12cUzl0psw5PocLVbVdXKFLtl/EoRT8+ZSY8tLnI7jhPnbWmjXP8U11r9miMI56xk2vrjJ2OztTqM8N9eOeFOP/sv6om67ETZK8k362a6NbYsYjNccX2NM5nov0bt2rokv0JytfPHKEJN/Pp8TaPSjs7iojfjnW8P+GTS+5xvQlo1LX3WdtWkljznOK+yZ2asucY4N2PjlXtwANGCFIUAAAAAJAAAUAAAABwkVGJkgqyRTEyAqMjFFQSpSFQFKQoAIAJV9GfnDfnRujaeuTi4xd0rI8sZhNKXLvWcrPh2H6PPEtv6P8fVq9Ra0tZBahQiuL8pTTKOZYf6Szw47SmWcxsn2116rnMsp8N13V0Sq01MnycqK8+XCnj6nW7z7Q01SzOcVnswm2d2k46atLsqgv9KNZ1GzVOfG0nNPKbWcHLcnXMWu37f00JxqupnFTrVkZTVcIutptSzKSXPHL5dTDaFstFNtRlBR4XKuyPDJRl0ku+PivI7qWxEpqbrrck8r2ej8jg7W2fLU2Qg1Fc/bkopTku7PXBPONTxlPbat2tT6+HE31jlHXbzaudLxGLeXhYwvqdvsSmNXDGKwlBI+u09mQ1CxLvyn2pmN6ybyc4tQ3Y3slc5VqlccVN8MrVGeIpPLykknnC59UbXs3art61yhn9o4+h2d+Hb4cPPVuCbZ2enp4nnH0LZWfCuONnvtxN7NJ67QayC978PY4/xKLa+qOn9C0IUU2W2RcZ6qyMKpPhUZQgnhdc+9xdnYbXdUnGUX0lFxa8GjrN1tmqOm2ZSnxSr9W5Ppw8nL7P6FtedxnX2ps1Y55d+pG/MhQdrzEAKSAAIAAEgUgAoAAAADglRCoKskZIxMkBUVEKglSkKBUAigAAEqeKb5zu0204V1JKLm1XFrKmrrJcafm5NeWD2o1Te3ZcJ2RtnXGcfVySk0uOmXJOcX2PH2Md065+nT+LnxlZfl9YRSrUF0jFRXb05HSXwcZeB2WzNVC2vMJqzhfBKS/aXXP3+JwdqSX2Oe9x0y8Uk1wt9Wlk1vYurjb+K1NlirjXa6lFtLEYxUpTk+7n9GdstZiLS6vkdV+DrTlKKcZT9/geFLzXQri0yd/szWQnJSUk4Po000/ijsFr9PK11Q1Ffr4rLq448fD3uPXBq2y9mVU8TrhKGZKTSzwt9/D0z4mw1aSpyVjilPv4VGT+OMi8L48vs7PaSaxnodlRDEc9pw9RBNLGE49PLuPrVY8JFE2l8uvgsme6mjlCcnPGUm+XNRXSK+WTFwcmkk5Puistrt5HebMocItyWJSeefXGOWfqbaseco5t2fjhl91zSAHY84AAFAAAAEgUgAoIUAAAOCioiKgqyKiIqCWRUQqApUQqAIpCgUEKQkRhqNPC2LhZCM4vqpLKMyoDqtdoq6q4KquFUI5jw1xUYrPPovHPzNW1sczfc19TetVBSrmn+y/muaNN1leX4pnPtnFdenLnFr20aXGFvD7yj7C65l2HXRevsrTkqdPHh5er4rJt+L5NGxanS5xJnHvb6dnyMp06J3Y13SUaiMm1dnviq5JvPLrl+Z3mno1cUmrU880lHOPPDPvptMpd6a8cHd6SnhWBc3TM+vTiaSWqfD631eM4fCmpPxZ2aWCeBPv9zL3VLeHabEhmc59kY4Xm/+EdycbZ2m9XWov3n7UvN9n9Dknfrx8cZHlbc/LO1AAXZgAAFIAKAAAAJApCgAABwUVGKMkFWSKjFGSApkjEoSyKiAClIUAUhQkKiFRArWU/Jmm6yRuaNH2usZXizDd8On8f5Yxui1wy7fkzqtXZGLwffRPjfC+f8AQ4W3NnSjFyi3xLPTpjsMXRHN0Vi4cprKO20upTSz1NA0b1bfDHp4mwaLQ6pJOU1FeRW8NJa2Sy2K5l0uXZW3yXHHHzRxNJpe1tyfezn6dflKv+5D7orPacvVbOyGRD0XkoAAAAAAAAUgAoAJAAAUEKBwEVERUFWSKjFGQGRURFCVKRFApUQoFAASIyQwVIgVGm7Xh7U/4pfc7x7xaWVl1FN0br6liyNalKFUn0Up44c/u5zy6HS65HPuvqOr8fG911GhxGzJztsyTgdZdlPPcy667MUZOnjt99nVRWOh29y7M/I6DZGoWcYzh8snew5/8FKvH1hHkZ6flZW/+pBv5osYnzt/qV9VN7nDamYnQW746Wq2FOpVumc8Ku6cM6ayWOcVOLeH4SS7+nM7rS6uq1N1XVXJdXVZGxL/AMWelLzOY8myy8V9SGRAhAUgAAAAABQQoAAEgAAOAVGJQqzRTEqAzRTFGSCWSKRFAqKEi/35ADJI1DeL0haHR5hCX4y9ZXBRJeri/wB6zovhl+B5lvJvxrtYmpW+oplnFGnfBHH70us/i8eCLTG0etbf310Gi4ozuV16ePUadqyxS7pPPDD+Zr4nle8m+Wp2hxOT9RVGfsUVTeIrHNyly45eLSXLklzzqlLws9y5Z7DJZ9XY+eeKLXhjPM0xxkRW3+jzbEKrbabHj101OE+xzxjhfmksHoGtmeG125x2Pry5c12o3Xd/epySp1EstcoWPlnwficm/T35R16d3XjWyahnGlz5GUrVLo8ow4OZyOt2WzNOl2I7uuCWDptEmsHcUrkVq8fUwccn2jE6DeneijQQayrdS17FKfTulPuQmNy6iuWUnddf6Q9o006WdMsTtuXDCPbHH6zww+nieebJ2rfpZxtpslCyLUk+bWP2Wu1PuOJtDW26i2V983Oc3ldiS7kuxIiWI8+eefTH9o9LRq/zx4edu2ed5etbA9K2nsShrKZ6excnZUnbTLxx70fL2vM3bZe2dLq1nTamq7llqEvbXnF818Ufmivkxpb5Qt9mUovqnGTjKL8GjS4Rk/UuCHhGy/SBtPTYX4j8TBfoaqPrf9XKf1Ny2R6WKJ4Wq01lLyk50yVta8Wnhr4ZK3CnL0UhxNmbV02qjx6e+u5YTfBL2o/xRfOPxRzCiUAAAAAUEKSAAA65GSIAqyRkmABUZoAJZIw1OprqhKy2yFVcfenZJQiviwBO6NI236TtNVxR0lUtTJcvWTzVQvJe9L5LzPPNub0a3XPF98vV5/M1ZrpXhwr3v5ssA3mMiGv6h8/7yZW+7X5MACwh7P8AaPtCv8nNAEocOjnJwb95cvCSM05Lrl9enX4oAhLtdnbdtrwlJWRXLhl1XhnsNr0O9WkcUrVOqeOb4XOD+Mef0AMstOGXfDXDdnj8u0r3j0KWfxKXg4WJ/LBjPf7RVJ8Ltul0UYVuK+csFBl+rg1/aza1tff/AFdycaVHSVvti1O1r+NrC+C+Jq0q3Juc3Jyby5zlmUn38+bAOjDXjj6jnyzyy9185e9GPjnzORfjp/8AAC0Vr5Kpvny5czifr4+T6ACkc2xHGs5Nd7fkAKR2Om1dlE1ZXZOmyPSdcnCa+J6puBv49TKOl1jX4iXKm5RUFdy92aXJS7muT8H1AjKSoegkAMFwAAAAABQSP//Z" >
                    <div class="card-info">
                        <h2>Mike Ross</h2>
                        <p class="member-position">Web Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div> 
                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdo0lwdIhjHTdksQ6vjyK4beyQVNSiHVyfEA&usqp=CAU" >
                    <div class="card-info">
                        <h2>Dan Star</h2>
                        <p class="member-position">Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKh-L6NubudwEma608y8v6nDx6ct4psSCqYA&usqp=CAU" >
                    <div class="card-info">
                        <h2>John Doe</h2>
                        <p class="member-position">CEO & Founder</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>

                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQt1QT3g-rGtN7u-KtGn7I_wUxnYYvKL7uqBA&usqp=CAU" >
                    <div class="card-info">
                        <h2>Anja Doe</h2>
                        <p class="member-position">Art Director</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://boldstudio.vn/wp-content/uploads/2019/09/chup-anh-chan-dung-22.jpg" >
                    <div class="card-info">
                        <h2>Mike Ross</h2>
                        <p class="member-position">Web Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div> 
                <div class="card">
                    <img class="card-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEA8PEBIQDxUPDw8QDxAVEA8PDw8VFREWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFRAQFy0dHR0rLS0tKy0tKy0tKystLS0tKy0vLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIEBQMGB//EAEIQAAIBAgMEBwQIBAMJAAAAAAABAgMRBCExBRJBYQYTIlFxgZEyUqHBByNCgpKx0fAUM2JyU7KzFSU0Q3N0osPx/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAfEQEBAQEAAwEBAQEBAAAAAAAAARECAyExQVESMhP/2gAMAwEAAhEDEQA/APlYwGetwCAaAoBoBgAAMgQ7DAAAdgCAB2CwAAWAAIkmQbAYCclxaXmCfdmFAAAQAAgpAMRACGAUgAAEhoEMqENAMIAAYAkSsJIkArDsKcklduyKNbHv7OXPiTrqT6slq9KSWbaRWqY2K9nPnwM6dRvN5gpHK+S346Tifq28XJ6N+SRBVZd8vVle7JK5nbWsixKvPi35OwliGtG/Vs4XFfw+A0yOkq7/AG2Lrmc2kRuZ1cWN7LOwlfVZedjhc6wkXTGhh6jaz4ZeJ1KeCfafwL8HHPeTeT3bNKz4N5O65ZeJ35uxx6mVAQwNMkAxBSAYECAACgAQyoEMQwgGkFixhaO83f2Ypym+S+bbS8WgIblvMjJpJybSt6s6VJ3bff6LuSM3aVXSK8X8v3zJ1cmnM2q2Jrub5LRdxXbBgeW3XokwAA0FNLuFdj3hlQhiGkArkt25KMDvSo3AquAkb+C2Yp2usuJV2zsadDtrtQbylxi3wf6lxVPCLtR8fkaRS2fHNvuRdO3jnpx7+kwJWIm2CAYBSsIkBBGwDGBFDEhlQIYhoCcUaFeHV0KcdJV/rZd6gm4015vfl+Er4DCutUp0Y61akIJ623mlfy18izt6vGeIquGUItU6a7oQShFeiQRnmLiZ3k3zNneav+7mHURy8v46+NAAA4OoAAAaY0RGa0TSJpHNE0VHWBfwazM+Bews88syj0eAhaz+BpKEZpwmlKMk009GmZuCTSV+JqU43NNPIVtnvDznT1W9eD748PPVeRE9Bt/C3gqi+xKz8Hl+djz515+PP3MoEMRWQAAAgGBFIYABAYIDSAkkRJxA9B0MpfX1az0w2FrVU+G80qcf9Rv7pg1Hdt97Z6vorStgdpVfeeHory35S/OJ5NmYhIxq8bNrubNkzdoQtK/fmY8s9OnjvtTAGB53YAAABJMIQcmlFOTeSSTbfgkOtRnB7s4yg+6UZRfoygTGmQJRLos0YXZt7Pw6jnYysAszfw6LCNPCwbtZFrGY+nh7KWbeqWbSsZlXH9XG0cm8r9x5zE3lJybbbd3nc1sg9NLb1GrvQfZU04vwfzMKpBxbi/stp+Rnxw7uaNRZtPPRfBG/H1tcvJP1zAbEdXIIAAgAGRYUAFwIIoAGaCJRIk4kHuOjdP8A3Pi3342S9KFL9TxE9X4s+g9D4b2x8Wu7HT+OHongcVG05LmTlmfa5FfG07xvxj+RYB6Wt33LZsxqXKw2I74qluya9PA4Hksx6IAQDiRWts7HSw0N6l2alS6dSyc4x92L4X1b1HiNp1an1deUqqfGb3pRb4qTzQYKkp078aWq4uLev75HHaj7aWSW7F+Nzf4KlWjutnJlly3lc41YkF3Zz+R6LCSTR5jBSsalDEtGoLe08LP2o3fIx472d8vI9HhcankznjsDCXai7X1RbP0ZeycLOdpN3vwLOOpuNSSeWj9Vc0tkpQSXFO5c6QYNVKSrRXaprNe9DV+mb9TXj9VnyTeXmWIQI7POYhkSBkRsQUDEAAAAADiIaA+k/Rk1UwO0qPGFWjV/HCUf/WeH25RcKsvE9Z9D+ItjMRh3picJK3OVKSkl+GVT0KPTjAblSTtozM+2MfK8gNLXlzS/+iHFaZ2552XobaV8dS3oXWsc/Lj++Rks3jHxNPdk16HDy8/rr47+OIIAOLquYSu4STXg+aepo9RTq7qnKVPdTzUVNtPO1m1bjmY1NlmlW0UruN87SUZW7lJp28bM3KJ1qCg2ozU43yycZea4epwqI7VKt1u3TzcrJJQi2krR9FnySzsmc2EKlkWIzOMUTQVap1LFqGJfeZ8WThIo2MNVs02z0lGr2E7Zc+PkeQo4l07VLX3WrX0vwZdxfSupNKPy18SyyfVqpjqHV1JRWl7x8Hmv08jgdK1d1GpS1tb4v9Tmd58eW/TuRYCAGIYgAAGAAAFACAANbottP+ExuExLyVKtF1H3U5dip/4SkfUPpE2TduSV75pnxs+3dF8Yto7JpOT3qmGX8NW4u8EtyT8YOL8WzHXqysdPiuJpOMmuZyPS9J9mOEpO3E80bWXQZ20Pa8kaLM/Ha+Rz8vx14+qQAwPO7JRZ0aOcUd4oojBHQViSRYhokiKRJASROBBDbKL9GrGzi9GjnGnu3XP4Gd1s75Zcy9Qgkr3b3rO71N8e6x38dAYAdnEgAGFIQyJAwACBgAzQQwABo9n9FvSFYPGqlUdqONUaNS+kJ3+qn6txfKd+B4sbV1YlmzEfaum+wfaaXM+SbQwEoz3UtXY+xfR90hjtTBvD1nfE4WKjNvWtT0jU5vhLnnxR43pjho0cT1S1jFSly3tF6L4k8fu5XP8A5rBwGGp0rPdU5LWTV8+V9C3tLY+Hxiu11VS2VSKt+JfaXxI0YXL9GlY9Fksxz2y7+vCbT6OV6F3KO/H/ABI5x8+7zKcMCmu4+q0pNFbE7Io1M4/Uyf2orst846P4HD/x5dp57+vndLYk52UJwbfBuxLF7FxFFb06b3V9uLU4eLa087Hp8RsmtTkrxTu7RlFpxefc7M5TjiKMruUqTem8moS5KWjF8XLU83Tx7Q0z0uMpUan8+l1Mn/z6VlFv+qGj+D5mFjtnzpZpqpHhOOnmtUcevHeXbnySuKA5JnRGGzuNEbDiUTpQvJLzZdS4FfDat9yLNjt4561x8l9kAxM6MEJjEyKQhsRAAAEEgJWFY2EMACABgBd2LtSrg8RSxVCW7OlK692aftQkuMWsn+qRvbUx8sXia+Jkt11p725e+4rJRjfjZJI8vQjeUVzVzfwzN8czdY7rRwtM0aUDhhI5GlRpo1XFGNIl1JajBE1AyKVst2SunweaMTaeDqUW503OUJe0laTh4p+0j08qRxcLZap6oK8U5QleLioO2e6t38VJ6eVjLxVF033xfdmn4fob3SPCzodtR6yi3o1vdTLz4PvPP1Kyksr24xzdvC+aFx05VZYOE81lzWq/UFSilayOyslk9f3cic/8z+Om1xlh0+RH+FXf8CwA/wAc/wAX/VQhBJWQyVgsaxnURNE7HapR6v2va9z3ecu58vXmTVVoiyciDI0REbEZUAIALFgsSsFjSIWCxKw7AQsFidgsVBQXaRt4JPIyKE92V2rribeB2phVbenu8mjXNkY73+N3AyyVzWw8U9DGw+2MD/jQXwNfB7Twkso1oPzQtjnlXHh3wOUoSRo0pJrJp+BKUbkGZGY2WqmHXAryp2A41qMZRlGSTUk009GuJ8929sl4aqrX3JXcG+K92/ej6MyttDAwr05Up6S0lxjLhJc0F56yvmDGaGI2LWpylBxu4u11ez7muRBbMre6yO2xSHY06OwcRPSD9GbGF6D4lx6yq4UIL2qlSShBebGyJseUsXMDsyrWu4R7MVedSTUKUF3ym8kjcqvZmF9ne2hUXdenhk+c3nL7qtzMfam2K2IsptRhF3hRgurow8IrV83d8xu/E0VatKjlRfWzWtdq0Y/9KLz++8+5LUzZMbZFlWIsiyTIMlVFkSTImWgAABcA5dYPrBqY6WGkc+sGqhdE7BYj1gb40Nwb7MdZZI2tldFqbSdW8stL2Rz6O4ffnKT+zZLxev5fE9lRopJGpzM2uXfdnqOWA2HhI2tQpt6XcVJ/E1aWBoq6VKja7y6uFrX8BYaGa8TvSWSGMbXD/ZNLWk54d/0S7H4HdeliaeIpe0o4iK4x7FRfdeT9fItROiZF1woYqFT2XmtYtWnHk080TlG4sRhozzazXsyWU4+D+WhXlVlT/mdqP+Lxj/evmvgESnTOMkWnK/nx4HKaKIUK8ablUnSddKDvTjuqpKyut3eyv495kVvpCwq/k7Pv3OpWUbeUYv8AM072Z4zphsrq5/xEF2Kr7dtITefpLXxvyM9cy+2uc+LmL+kLGyuqSw+FT06ukpTX3qm9+SPN4/H1q8t+vVqVnwc5ynbwT08ir5jtzEkjpgbItkrLvFu8zWiDEybjzItcxogyLJtEGiVUGIk0RMtAAACG+HWFfeHvGdbxZVQOsK6ZJMamLKmS3yqmWsFS6ypCn78knyXH4XLEr3HRrDdXRg2s59t/e0+FjegZ+HqLhord2RepcDvXk33q7QWvg/jl8yxGJzpLLxa/f5FlIwrmTiyVgsA0xSiK4bxBn1KTpZwV4aunq484fp6dx0jNSSkndPRlqRQrU3TbnFXi3ecF8ZLn+/GgmjlWoQqwnSqK8ZqzWj5NdzWvkdrppNO6aun3kGrFR8x2jgp4erOjPWDydrKUeEl4ornu+l2zOvo9bBfWUE3znDWUfFarz7zwKZi+nfm7EwIBcmtYmIjcLjQ2RYXFcmhNEWiTENVGwDsMaM+4XIXGmYdXRMkmcrjuEdkzW6NwvXv7kJPzfZ+bMVM9J0VpdmpP3pKC8ld/5vgdOPfTn5PXNeuwkczYw9PS5lYepGEbyKuM21bKJ1ryyPUqrFWzSO0MTHvTPm+I2vJ8X6kKe2pp6kyNZX1GM0wkeCwXSSSte7+J6HA7ejPJ5jEa8mQ3xxmprss4SkyI7qYSK2+TUwK84bjdvZk9PdfeuRKTOlTtIqU5/Zf3f0KOm9Y8J0uwnVVYW3tySk6au3CCurxitI5tvLW6Pcy0M7bmz/4mhOml2126T/rXDzV15ks2NcXK+c7wbxC/l3rRiucnoT3hXIXC4VK4rkbhcCVxEbhcCQEbiAoIaADm6GMYFAj1/Rv+TT/un/mYAdfF9cfN8bOM08kYWJ1kIDrXDlQqakFr5MYGHZ1pamrhNAA1y59PZbF9leLL+L1f74AArmrz1OtPRAAQU9ClW9pf3/IACuz4kY+0vEAA+X7V/wCIxH/cVv8AUkVQA5V6p8DIgBFAgAigQAAAAAf/2Q==" >
                    <div class="card-info">
                        <h2>Dan Star</h2>
                        <p class="member-position">Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjladW6wE4Ky6obz061AbMsWN75T5-_h4IJw&usqp=CAU" >
                    <div class="card-info">
                        <h2>Anja Doe</h2>
                        <p class="member-position">Art Director</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"><i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div>
                <div class="card">
                    <img class="card-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw0PDw0ODQ0QDQ8ODw8NDQ8NDQ0PFRUWFhURFRUYHSggGBolGxYXITEhJSkrLi4uFyAzODMsNygtLisBCgoKDg0OFhAQGi0dHR0tLS0tLS0tLSstKy03LS0tLS0rLS0rLS0rLS0tLi0tLSstLS0tLSsrLS0tLS0tLS0rLf/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAEBAAIDAQEAAAAAAAAAAAAAAQIGBAUHAwj/xAA/EAACAgEBBQQIAQoFBQAAAAAAAQIDEQQFBhIhMUFRYXEHEyIygZGhsRQjM0JDUmJygsHwFSSi0eFjc5LC8f/EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EACERAQEAAgICAgMBAAAAAAAAAAABAgMRIRIxQVEEFCIT/9oADAMBAAIRAxEAPwD1NFIUlmpSFQSqAAAAoAAAACgACgAUqRCUwMHE2ntXT6WHHqLoUw6ZnJLLNN236VNDTFrTf5y1NLhXHVW/5+F/RMDfcFweSQ9M/tRzoPZ6SUb+PHipcKz5NI7/AGJ6Vdn6iShdx6OT6St9qlvu410+KS8SThveASmyM4qUJRnCSypRalFrvTRkBiCgCAoAAAAAABSFAAAAAAABQOKikKFVKQBLIBAAUhQAKAABQBUiI4e2tp16PT26m14rrjxPCy5PsivFsJZ7V2nRpKZ36m2NNMOspd/ZFJc232Jc2eL76elS/UOVWgc9Np1y9cpSr1Fq7enuL6+Rqm9+9Oq2ndx6ixeqhKSppr5VVrvSzzlj9J/Y6H6BPD62XSnJzm3Ob6ym3KT+L5keew+aku/5maljt+QSkoeb+eQn3t/Ern/ayYcXhkgbFuvvjrtmtKi7NDlxOixKdEn2tLrHP7rXxPbt3d+tLrKI259VNSjC2uT/ADc34/svsfhjqfm9PHfjufYczZ20Z0ObjhqScZprKcX2PPLtYRY/VdV8ZPCfPCl5p9p9TwvcLfyyF+mo1EnKpZgpv2pxg02od7SeH3rB7hRdGcVKElKLSaaeU0+0lDMFIAAAAAoAAAAAAAKAAAHERSFCqlIUJVFIigCkKBQAAKABUeKembeh23/4fXhU0NStk1mU7WspeCSfZ14n2dfYNra6Om0+o1EucaabLWu/gi3j6H5X2hqJ3WWW2ScrLLJTnJ9ZTk25P5hMcVzObs/Ztt/OK9n9pr7Hw0OldtkYd7PXdn7HjVVCKj+ijLZn4+m+vX5e3ntW7DfvSfwR2FG7Va6xz5m5x0az0OVVoV3HPdtdOOnFrGk3dqS/NpvxOzo3Y07/AFMcmxV6VI51GnWDO7K1mufTT9TuhS1+bXlzweebzbElpLOX5t8128Phk93lT8Ua/vPsOOoqnHC4uF4fiX17bL2z2aZZ08Wpsw1LmsY93qmeu+ireSHDLTqy6Vi9v/MTU1KHdFZcks56Z97yx5FdU65zhJYcW0bBuFGUtfpq4SjGc5vglNNxjLhk8PHPD6fHwO159j9Kae5TipJNeDWGfQ+WlhwwXs8Dxlx5PDfXofYIQpASKACAABIAAAUAAAAOGioxRkFVRkYooSyKYmQBFIUCoAoAIBEJdJvxQ7NmbQhHq9LZ9Fl/RH5kfP5H601FKshOuXuzhKEvKSw/uflW6hwttrl1rsnB+PC8fLkEx2W52k49VTHGW5fBJHsk6OFY+hpPo42O1bXdJfqZT8uJrH9TaN5ttR0y5R47MZ4V0j5/7HNs7rr1/wAzthKHPociqvK6GkLfaWVxaezGebzhfA2rYW36dQkk5Rl3Si0/9jLLXY3x2Y12cUzl0psw5PocLVbVdXKFLtl/EoRT8+ZSY8tLnI7jhPnbWmjXP8U11r9miMI56xk2vrjJ2OztTqM8N9eOeFOP/sv6om67ETZK8k362a6NbYsYjNccX2NM5nov0bt2rokv0JytfPHKEJN/Pp8TaPSjs7iojfjnW8P+GTS+5xvQlo1LX3WdtWkljznOK+yZ2asucY4N2PjlXtwANGCFIUAAAAAJAAAUAAAABwkVGJkgqyRTEyAqMjFFQSpSFQFKQoAIAJV9GfnDfnRujaeuTi4xd0rI8sZhNKXLvWcrPh2H6PPEtv6P8fVq9Ra0tZBahQiuL8pTTKOZYf6Szw47SmWcxsn2116rnMsp8N13V0Sq01MnycqK8+XCnj6nW7z7Q01SzOcVnswm2d2k46atLsqgv9KNZ1GzVOfG0nNPKbWcHLcnXMWu37f00JxqupnFTrVkZTVcIutptSzKSXPHL5dTDaFstFNtRlBR4XKuyPDJRl0ku+PivI7qWxEpqbrrck8r2ej8jg7W2fLU2Qg1Fc/bkopTku7PXBPONTxlPbat2tT6+HE31jlHXbzaudLxGLeXhYwvqdvsSmNXDGKwlBI+u09mQ1CxLvyn2pmN6ybyc4tQ3Y3slc5VqlccVN8MrVGeIpPLykknnC59UbXs3art61yhn9o4+h2d+Hb4cPPVuCbZ2enp4nnH0LZWfCuONnvtxN7NJ67QayC978PY4/xKLa+qOn9C0IUU2W2RcZ6qyMKpPhUZQgnhdc+9xdnYbXdUnGUX0lFxa8GjrN1tmqOm2ZSnxSr9W5Ppw8nL7P6FtedxnX2ps1Y55d+pG/MhQdrzEAKSAAIAAEgUgAoAAAADglRCoKskZIxMkBUVEKglSkKBUAigAAEqeKb5zu0204V1JKLm1XFrKmrrJcafm5NeWD2o1Te3ZcJ2RtnXGcfVySk0uOmXJOcX2PH2Md065+nT+LnxlZfl9YRSrUF0jFRXb05HSXwcZeB2WzNVC2vMJqzhfBKS/aXXP3+JwdqSX2Oe9x0y8Uk1wt9Wlk1vYurjb+K1NlirjXa6lFtLEYxUpTk+7n9GdstZiLS6vkdV+DrTlKKcZT9/geFLzXQri0yd/szWQnJSUk4Po000/ijsFr9PK11Q1Ffr4rLq448fD3uPXBq2y9mVU8TrhKGZKTSzwt9/D0z4mw1aSpyVjilPv4VGT+OMi8L48vs7PaSaxnodlRDEc9pw9RBNLGE49PLuPrVY8JFE2l8uvgsme6mjlCcnPGUm+XNRXSK+WTFwcmkk5Puistrt5HebMocItyWJSeefXGOWfqbaseco5t2fjhl91zSAHY84AAFAAAAEgUgAoIUAAAOCioiKgqyKiIqCWRUQqApUQqAIpCgUEKQkRhqNPC2LhZCM4vqpLKMyoDqtdoq6q4KquFUI5jw1xUYrPPovHPzNW1sczfc19TetVBSrmn+y/muaNN1leX4pnPtnFdenLnFr20aXGFvD7yj7C65l2HXRevsrTkqdPHh5er4rJt+L5NGxanS5xJnHvb6dnyMp06J3Y13SUaiMm1dnviq5JvPLrl+Z3mno1cUmrU880lHOPPDPvptMpd6a8cHd6SnhWBc3TM+vTiaSWqfD631eM4fCmpPxZ2aWCeBPv9zL3VLeHabEhmc59kY4Xm/+EdycbZ2m9XWov3n7UvN9n9Dknfrx8cZHlbc/LO1AAXZgAAFIAKAAAAJApCgAABwUVGKMkFWSKjFGSApkjEoSyKiAClIUAUhQkKiFRArWU/Jmm6yRuaNH2usZXizDd8On8f5Yxui1wy7fkzqtXZGLwffRPjfC+f8AQ4W3NnSjFyi3xLPTpjsMXRHN0Vi4cprKO20upTSz1NA0b1bfDHp4mwaLQ6pJOU1FeRW8NJa2Sy2K5l0uXZW3yXHHHzRxNJpe1tyfezn6dflKv+5D7orPacvVbOyGRD0XkoAAAAAAAAUgAoAJAAAUEKBwEVERUFWSKjFGQGRURFCVKRFApUQoFAASIyQwVIgVGm7Xh7U/4pfc7x7xaWVl1FN0br6liyNalKFUn0Up44c/u5zy6HS65HPuvqOr8fG911GhxGzJztsyTgdZdlPPcy667MUZOnjt99nVRWOh29y7M/I6DZGoWcYzh8snew5/8FKvH1hHkZ6flZW/+pBv5osYnzt/qV9VN7nDamYnQW746Wq2FOpVumc8Ku6cM6ayWOcVOLeH4SS7+nM7rS6uq1N1XVXJdXVZGxL/AMWelLzOY8myy8V9SGRAhAUgAAAAABQQoAAEgAAOAVGJQqzRTEqAzRTFGSCWSKRFAqKEi/35ADJI1DeL0haHR5hCX4y9ZXBRJeri/wB6zovhl+B5lvJvxrtYmpW+oplnFGnfBHH70us/i8eCLTG0etbf310Gi4ozuV16ePUadqyxS7pPPDD+Zr4nle8m+Wp2hxOT9RVGfsUVTeIrHNyly45eLSXLklzzqlLws9y5Z7DJZ9XY+eeKLXhjPM0xxkRW3+jzbEKrbabHj101OE+xzxjhfmksHoGtmeG125x2Pry5c12o3Xd/epySp1EstcoWPlnwficm/T35R16d3XjWyahnGlz5GUrVLo8ow4OZyOt2WzNOl2I7uuCWDptEmsHcUrkVq8fUwccn2jE6DeneijQQayrdS17FKfTulPuQmNy6iuWUnddf6Q9o006WdMsTtuXDCPbHH6zww+nieebJ2rfpZxtpslCyLUk+bWP2Wu1PuOJtDW26i2V983Oc3ldiS7kuxIiWI8+eefTH9o9LRq/zx4edu2ed5etbA9K2nsShrKZ6excnZUnbTLxx70fL2vM3bZe2dLq1nTamq7llqEvbXnF818Ufmivkxpb5Qt9mUovqnGTjKL8GjS4Rk/UuCHhGy/SBtPTYX4j8TBfoaqPrf9XKf1Ny2R6WKJ4Wq01lLyk50yVta8Wnhr4ZK3CnL0UhxNmbV02qjx6e+u5YTfBL2o/xRfOPxRzCiUAAAAAUEKSAAA65GSIAqyRkmABUZoAJZIw1OprqhKy2yFVcfenZJQiviwBO6NI236TtNVxR0lUtTJcvWTzVQvJe9L5LzPPNub0a3XPF98vV5/M1ZrpXhwr3v5ssA3mMiGv6h8/7yZW+7X5MACwh7P8AaPtCv8nNAEocOjnJwb95cvCSM05Lrl9enX4oAhLtdnbdtrwlJWRXLhl1XhnsNr0O9WkcUrVOqeOb4XOD+Mef0AMstOGXfDXDdnj8u0r3j0KWfxKXg4WJ/LBjPf7RVJ8Ltul0UYVuK+csFBl+rg1/aza1tff/AFdycaVHSVvti1O1r+NrC+C+Jq0q3Juc3Jyby5zlmUn38+bAOjDXjj6jnyzyy9185e9GPjnzORfjp/8AAC0Vr5Kpvny5czifr4+T6ACkc2xHGs5Nd7fkAKR2Om1dlE1ZXZOmyPSdcnCa+J6puBv49TKOl1jX4iXKm5RUFdy92aXJS7muT8H1AjKSoegkAMFwAAAAABQSP//Z" >
                    <div class="card-info">
                        <h2>Mike Ross</h2>
                        <p class="member-position">Web Designer</p>
                        <p class="member-speech">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p class="contact"> <i class="fa fa-envelope"></i> Contact </p>
                    </div>
                </div> 
            </div>
        </div>
        
        
        <div class="pagination">
                
                <li class="page-item previous-page disable"><a class="page-link" href="#">Prev</a></li>
                <li class="page-item current-page active"><a class="page-link" href="#">1</a></li>
                <li class="page-item dots"><a class="page-link" href="#">...</a></li>
                <li class="page-item current-page"><a class="page-link" href="#">5</a></li>
                <li class="page-item current-page"><a class="page-link" href="#">6</a></li>
                <li class="page-item dots"><a class="page-link" href="#">...</a></li>
                <li class="page-item current-page"><a class="page-link" href="#">10</a></li>
                <li class="page-item next-page"><a class="page-link" href="#">Next</a></li>
        </div>

    
        <script type="text/javascript">
            function getPageList(totalPages, page, maxLength){
              function range(start, end){
                return Array.from(Array(end - start + 1), (_, i) => i + start);
              }
            
              var sideWidth = maxLength < 9 ? 1 : 2;
              var leftWidth = (maxLength - sideWidth * 2 - 3) >> 1;
              var rightWidth = (maxLength - sideWidth * 2 - 3) >> 1;
            
              if(totalPages <= maxLength){
                return range(1, totalPages);
              }
            
              if(page <= maxLength - sideWidth - 1 - rightWidth){
                return range(1, maxLength - sideWidth - 1).concat(0, range(totalPages - sideWidth + 1, totalPages));
              }
            
              if(page >= totalPages - sideWidth - 1 - rightWidth){
                return range(1, sideWidth).concat(0, range(totalPages- sideWidth - 1 - rightWidth - leftWidth, totalPages));
              }
            
              return range(1, sideWidth).concat(0, range(page - leftWidth, page + rightWidth), 0, range(totalPages - sideWidth + 1, totalPages));
            }
            
            $(function(){
              var numberOfItems = $(".card-content .card").length;
              var limitPerPage = 3; //How many card items visible per a page
              var totalPages = Math.ceil(numberOfItems / limitPerPage);
              var paginationSize = 7; //How many page elements visible in the pagination
              var currentPage;
            
              function showPage(whichPage){
                if(whichPage < 1 || whichPage > totalPages) return false;
            
                currentPage = whichPage;
            
                $(".card-content .card").hide().slice((currentPage - 1) * limitPerPage, currentPage * limitPerPage).show();
            
                $(".pagination li").slice(1, -1).remove();
            
                getPageList(totalPages, currentPage, paginationSize).forEach(item => {
                  $("<li>").addClass("page-item").addClass(item ? "current-page" : "dots")
                  .toggleClass("active", item === currentPage).append($("<a>").addClass("page-link")
                  .attr({href: "javascript:void(0)"}).text(item || "...")).insertBefore(".next-page");
                });
            
                $(".previous-page").toggleClass("disable", currentPage === 1);
                $(".next-page").toggleClass("disable", currentPage === totalPages);
                return true;
              }
            
              $(".pagination").append(
                $("<li>").addClass("page-item").addClass("previous-page").append($("<a>").addClass("page-link").attr({href: "javascript:void(0)"}).text("Prev")),
                $("<li>").addClass("page-item").addClass("next-page").append($("<a>").addClass("page-link").attr({href: "javascript:void(0)"}).text("Next"))
              );
            
              $(".card-content").show();
              showPage(1);
            
              $(document).on("click", ".pagination li.current-page:not(.active)", function(){
                return showPage(+$(this).text());
              });
            
              $(".next-page").on("click", function(){
                return showPage(currentPage + 1);
              });
            
              $(".previous-page").on("click", function(){
                return showPage(currentPage - 1);
              });
            });
        </script>
                  

    
    
    <div class="number-container">
        <div class="number-info">
            <p> <span style="font-size: 36px;">14+</span> <br/>Partners</p>
        </div>
        <div class="number-info">
            <p> <span style="font-size: 36px;">55+</span> <br/>Projects Done</p>          
        </div>
        <div class="number-info">
            <p> <span style="font-size: 36px;">89+</span> <br/>Happy Clients</p>
        </div>
        <div class="number-info">
            <p> <span style="font-size: 36px;">150+</span> <br/>Meetings</p>
        </div>
    </div>

    <div id="WORK">
        <h2>OUR WORK</h2>
        <h3>What we've done for people</h3>
        <div class="our-work-picture-grid">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_mic.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_phone.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_drone.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_sound.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_tablet.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_camera.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_typewriter.jpg">
            <img class="our-work-picture" src="https://www.w3schools.com/w3images/tech_tableturner.jpg">
        
        </div>

    </div>

    <div class="our-skills">
        <div class="our-skill-text">
            <h2>Our Skills</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br/>
                tempor incididunt ut labore et dolore.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br/>
                tempor incididunt ut labore et dolore.</p>
        </div>
        <div class="our-skill-percent">
            <article><i class="fa fa-camera w3-margin-right"></i>Photography</article>
            <div class="percent"><p style="width: 90%;">90%</p></div>
            <article><i class="fa fa-desktop w3-margin-right"></i>Web Design</article>
            <div class="percent"><p style="width: 85%;">85%</p></div>
            <article><i class="fa fa-photo w3-margin-right"></i>Photoshop</article>
            <div class="percent"><p style="width: 75%;">75%</p></div>
        </div>
    </div>

    <div id="PRICING">
        <h2>PRICING</h2>
        <h3>Choose a pricing plan that fits your needs.</h3>
        <div class="pricing-table">
            <div class="table-1">
                <h2>Basic</h2>
                <p><b>10GB</b> Storage</p>
                <p><b>10</b> Emails</p>
                <p><b>10</b> Domains</p>
                <p><b>Endless</b> Support</p>
                <p><span style="font-size: 30px;">$ 10</span><br/><span style="color: grey;">per month</span></p>
                <div class="sign-up"><div class="sign-up-button">Sign Up</div></div>
            </div>

            <div class="table-2">
                <h2>Pro</h2>
                <p><b>25GB</b> Storage</p>
                <p><b>25</b> Emails</p>
                <p><b>25</b> Domains</p>
                <p><b>Endless</b> Support</p>
                <p><span style="font-size: 30px;">$ 25</span><br/><span style="color: grey;">per month</span></p>
                <div class="sign-up"><div class="sign-up-button">Sign Up</div></div>
            </div>
            <div class="table-1">
                <h2>Premium</h2>
                <p><b>50GB</b> Storage</p>
                <p><b>50</b> Emails</p>
                <p><b>50</b> Domains</p>
                <p><b>Endless</b> Support</p>
                <p><span style="font-size: 30px;">$ 50</span><br/><span style="color: grey;">per month</span></p>
                <div class="sign-up"><div class="sign-up-button">Sign Up</div></div>
            </div>
        </div>
    
        
    </div>

    <div id="CONTACT">
        <h2>CONTACT</h2>
        <h3>Lets get in touch. Send us a message:</h3>
        <ul class="info-to-contact">
            <li><i class="fa fa-map-marker fa-fw" style="font-size:36px;"></i>Chicago, US</li>
            <li><i class="fa fa-phone fa-fw" style="font-size:36px;"></i>Phone: +00 151515</li>
            <li><i class="fa fa-envelope fa-fw" style="font-size:36px;"> </i>Email: mail@gmail.com</li>
        </ul>

        <!-- VALIDATE FORM--------------------------------->
        <form class="contact-form" id="form-2" action ="./WEB1contact-sql.php" method = "post">
            <table class="contact-input-table" >
                <input class="table-input" type="text" name="name" value="" id="name" placeholder="Name" >
            
                <input class="table-input" type="text" name="email" id="email" value="" placeholder="Email" >
             
                <input class="table-input" type="text" name="phoneNumber" id="phoneNumber" value="" placeholder="Phone Number" >
            
                <textarea class="table-input" id="message"  name="message" placeholder="Message" ></textarea>
            </table>
        
            <button id = "send" name="send" type="send" value="send" >
                <i class="fa fa-paper-plane" value="send" name="send" ></i>
                SEND MESSAGE
            </button>

        </form>


        <!----------------------------------------------------->

        <img style ="filter: grayscale(65%);" src="https://www.w3schools.com/w3images/map.jpg">
    </div>

    <div class="footer">
        <a href="#HEADING" class="to-the-top-button"><i class="fa fa-arrow-up w3-margin-right"></i> To the top</a>
        <div class="SNS-logo" style="color: white; ">
            <i class="fab fa-facebook fa-lg" aria-hidden="true"></i>
            <i class="fab fa-instagram fa-lg" aria-hidden="true"></i>
            <i class="fab fa-snapchat fa-lg" aria-hidden="true"> </i>
            <i class="fab fa-pinterest fa-lg" aria-hidden="true"> </i>
            <i class="fab fa-twitter fa-lg" aria-hidden="true"></i>
            <i class="fab fa-linkedin fa-lg" aria-hidden="true"></i>
        </div>
        <p >Made by <a style="color: white;" href="https://www.facebook.com/hndt2725/">Hồng Nhung</a></p>
    </div>


    <!-- <script src="./WEB1-java.js"></script>
    <script>
        Validator({
            form: "#form-2",
        formGroupSelector: '.form-group', // thay tên class tương ứng là được
        errorSelector:'.form-message' , // -> .js : thay vì dùng "form-message" thì dùng "option.errorSelector"
        rules: [  
          Validator.isRequired('#name','Please enter your full name.'), 
          Validator.isRequired('#email', 'Please enter your email'),
          Validator.isEmail('#email', 'Invalid email format'),
          Validator.isRequired('#phoneNumber', 'Plese enter your phone number'),
          Validator.isPhonenumber('#phoneNumber', 'Invalid phone-number format'),
          
        ],
        })
      </script> -->



</body>
</html>